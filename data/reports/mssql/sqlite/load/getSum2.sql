SELECT
	 t.[HostName]    AS [HostName]
	,t.[RecordSet]   AS [RecordSet]
	,t.[CurrentTime] AS [CurrentTime]
	,t.[Solution]    AS [Solution]
FROM
	[${getSum2}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[HostName] ASC
;
