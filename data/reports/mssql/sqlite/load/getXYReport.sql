SELECT
	 t.[someVar1] AS [someVar1]
	,t.[someVar2] AS [someVar2]
FROM
	[${getXYReport}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[someVar1] ASC
;
