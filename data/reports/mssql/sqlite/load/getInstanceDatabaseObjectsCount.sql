SELECT
	 t.[TypeDesc]      AS [TypeDesc]
	,t.[IsMSShipped]   AS [IsMSShipped]
	,t.[IsIssue]       AS [IsIssue]
	,t.[TypeCount]     AS [TypeCount]
	,t.[CreateDateMin] AS [CreateDateMin]
	,t.[CreateDateMax] AS [CreateDateMax]
FROM
	[${getInstanceDatabaseObjectsCount}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[TypeDesc] ASC
;
