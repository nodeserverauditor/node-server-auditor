SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 @@SERVERNAME      AS [ServerName]
	,@@VERSION         AS [ServerVersion]
	,SUSER_NAME()      AS [SUserName]
	,USER_NAME()       AS [UserName]
	,@strDateValue     AS [EventTime]
	,@strDescription   AS [Message]
	,@intNumberOfHours AS [RecordSet]
;

SELECT
	 @@SERVERNAME    AS [ServerName]
	,@@VERSION       AS [ServerVersion]
	,SUSER_NAME()    AS [SUserName]
	,USER_NAME()     AS [UserName]
	,GETDATE()       AS [EventTime]
	,@strDescription AS [Message]
	,2               AS [RecordSet]
;
