SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@intIndex [INTEGER]
;

CREATE TABLE #t1
(
	 [RecordSet] [INTEGER]        NOT NULL
	,[Name]      [NVARCHAR](128)  NOT NULL
	,[Value1]    [DECIMAL](24, 4) NOT NULL
	,[Value2]    [DECIMAL](24, 4) NOT NULL
	,[Value3]    [DECIMAL](24, 4) NOT NULL
	,[Value4]    [DECIMAL](24, 4) NOT NULL
);

SET @intIndex = 1;

WHILE (@intIndex <= 10)
BEGIN
	INSERT INTO #t1
	(
		 [RecordSet]
		,[Name]
		,[Value1]
		,[Value2]
		,[Value3]
		,[Value4]
	)
	VALUES
	(
		 1
		,RIGHT(N'000000' + CONVERT([NVARCHAR](128), @intIndex), 6)
		,-10.0 + 20.0 * RAND()
		,-15.0 + 30.0 * RAND()
		,-12.0 + 25.5 * RAND()
		,-18.0 + 15.5 * RAND()
	);

	INSERT INTO #t1
	(
		 [RecordSet]
		,[Name]
		,[Value1]
		,[Value2]
		,[Value3]
		,[Value4]
	)
	VALUES
	(
		 2
		,RIGHT(N'000000' + CONVERT([NVARCHAR](128), @intIndex), 6)
		,-10.0 + 20.0 * RAND()
		,-15.0 + 30.0 * RAND()
		,-12.0 + 25.5 * RAND()
		,0.0
	);

	SET @intIndex = @intIndex + 1;
END

SELECT
	 t.[Name]   AS [Name]
	,t.[Value1] AS [Value1]
FROM
	#t1 t
WHERE
	t.[RecordSet] = 1
ORDER BY
	t.[Value3] ASC
;

SELECT
	 t.[Name]   AS [Name]
	,t.[Value1] AS [Value1]
	,t.[Value2] AS [Value2]
	,t.[Value3] AS [Value3]
FROM
	#t1 t
WHERE
	t.[RecordSet] = 2
ORDER BY
	t.[Value1] ASC
;
