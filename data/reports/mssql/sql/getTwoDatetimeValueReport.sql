SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@intIndex [INTEGER]
;

CREATE TABLE #t1
(
	 [EventDateTime] [DATETIME]       NOT NULL
	,[EventValue1]   [DECIMAL](24, 4) NOT NULL
	,[EventValue2]   [DECIMAL](24, 4) NOT NULL
);

CREATE TABLE #t2
(
	 [EventDateTime] [DATETIME]       NOT NULL
	,[EventValue1]   [DECIMAL](24, 4) NOT NULL
	,[EventValue2]   [DECIMAL](24, 4) NOT NULL
);

CREATE TABLE #t3
(
	 [EventDateTime] [DATETIME]       NOT NULL
	,[EventValue1]   [DECIMAL](24, 4) NOT NULL
	,[EventValue2]   [DECIMAL](24, 4) NOT NULL
);

SET @intIndex = 1;

WHILE (@intIndex <= 10)
BEGIN
	INSERT INTO #t1
	(
		 [EventDateTime]
		,[EventValue1]
		,[EventValue2]
	)
	VALUES
	(
		DATEADD(mi, CONVERT([INTEGER], -5.0 * 10.0 * RAND()), GETDATE())
		,-10.0 + 20.0 * RAND()
		,-10.0 + 20.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SET @intIndex = 1;

WHILE (@intIndex <= 20)
BEGIN
	INSERT INTO #t2
	(
		 [EventDateTime]
		,[EventValue1]
		,[EventValue2]
	)
	VALUES
	(
		DATEADD(hh, CONVERT([INTEGER], -5.0 * 10.0 * RAND()), GETDATE())
		,-10.0 + 20.0 * RAND()
		,-10.0 + 20.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SET @intIndex = 1;

WHILE (@intIndex <= 20)
BEGIN
	INSERT INTO #t3
	(
		 [EventDateTime]
		,[EventValue1]
		,[EventValue2]
	)
	VALUES
	(
		DATEADD(dd, CONVERT([INTEGER], -5.0 * 10.0 * RAND()), GETDATE())
		,-10.0 + 20.0 * RAND()
		,-10.0 + 20.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SELECT
	 t.[EventDateTime] AS [EventDateTime]
	,t.[EventValue1]   AS [EventValue1]
	,t.[EventValue2]   AS [EventValue2]
FROM
	#t1 t
ORDER BY
	t.[EventDateTime] ASC
;

SELECT
	 t.[EventDateTime] AS [EventDateTime]
	,t.[EventValue1]   AS [EventValue1]
	,t.[EventValue2]   AS [EventValue2]
FROM
	#t2 t
ORDER BY
	t.[EventDateTime] ASC
;

SELECT
	 t.[EventDateTime] AS [EventDateTime]
	,t.[EventValue1]   AS [EventValue1]
	,t.[EventValue2]   AS [EventValue2]
FROM
	#t3 t
ORDER BY
	t.[EventDateTime] ASC
;
