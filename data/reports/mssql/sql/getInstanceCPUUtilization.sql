SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 @@ServerName                                  AS [ServerName]
	,y.[RecordEventTime]                           AS [EventTime]
	,y.[RecordTimeStamp]                           AS [RecordTimeStamp]
	,y.[RingBufferType]                            AS [RecordRingBufferType]
	,y.[MemoryUtilization]                         AS [MemoryUtilization]
	,y.[SystemIdle]                                AS [SystemIdle]
	,y.[ProcessUtilization]                        AS [SQLProcessCPUUtilization]
	,100 - y.[SystemIdle] - y.[ProcessUtilization] AS [OtherProcessesCPUUtilization]
FROM
	(
		SELECT
			 x.[RecordEventTime]                                                                                 AS [RecordEventTime]
			,x.[RecordTimeStamp]                                                                                 AS [RecordTimeStamp]
			,x.[RingBufferType]                                                                                  AS [RingBufferType]
			,XMLRecord.value('(//Record/@id)[1]', '[INTEGER]')                                                   AS [RecordId]
			,XMLRecord.value('(//Record/@type)[1]', '[NVARCHAR](128)')                                           AS [RecordType]
			,XMLRecord.value('(//Record/@time)[1]', '[BIGINT]')                                                  AS [RecordTime]
			,XMLRecord.value('(./Record/SchedulerMonitorEvent/SystemHealth/SystemIdle)[1]', '[INTEGER]')         AS [SystemIdle]
			,XMLRecord.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', '[INTEGER]') AS [ProcessUtilization]
			,XMLRecord.value('(./Record/SchedulerMonitorEvent/SystemHealth/MemoryUtilization)[1]', '[BIGINT]')   AS [MemoryUtilization]
		FROM (
			SELECT
				 DATEADD(ms, t.[timestamp] - tSDOSI.[ms_ticks], GETDATE()) AS [RecordEventTime]
				,t.[timestamp]                                             AS [RecordTimeStamp]
				,t.[ring_buffer_type]                                      AS [RingBufferType]
				,CONVERT(XML, t.[record])                                  AS [XMLRecord]
			FROM
				[sys].[dm_os_ring_buffers] t
				CROSS JOIN [sys].[dm_os_sys_info] AS tSDOSI
			WHERE
				t.[ring_buffer_type] = N'RING_BUFFER_SCHEDULER_MONITOR'
				AND t.[record] LIKE '% %'
				AND (tSDOSI.[ms_ticks] - t.[timestamp]) < 2147483647
		) AS x
	) AS y
ORDER BY
	y.[RecordEventTime] ASC
;
