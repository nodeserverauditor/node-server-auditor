SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@intMySpId [INTEGER]
;

SET @intMySpId = @@spid;

SELECT
	 tSDEC.[session_id]                                                   AS [SessionID]
	,tSDES.[login_name]                                                   AS [SessionLoginName]
	,tSDEC.[connect_time]                                                 AS [SessionConnectTime]
	,tSDEC.[auth_scheme]                                                  AS [SessionAuthScheme]
	,tSDEC.[net_transport]                                                AS [SessionNetTransport]
	,tSDEC.[protocol_type]                                                AS [SessionProtocolType]
	,tSDEC.[client_net_address]                                           AS [SessionClientNetAddress]
	,tSDES.[host_name]                                                    AS [SessionHostName]
	,tSDES.[client_interface_name]                                        AS [SessionClientInterfaceName]
	,tSDER.[command]                                                      AS [RequestCommand]
	,tSDER.[start_time]                                                   AS [RequestStartTime]
	,tSDER.[percent_complete]                                             AS [RequestPercentComplete]
	,tSDER.[estimated_completion_time] / 1000                             AS [RequestSecondsToGo]
	,DATEADD(SECOND, tSDER.[estimated_completion_time] / 1000, getdate()) AS [RequestEstimatedCompletionTime]
FROM
	[sys].[dm_exec_connections] tSDEC
	LEFT OUTER JOIN [sys].[dm_exec_sessions] tSDES ON
		tSDES.[session_id] = tSDEC.[session_id]
	LEFT OUTER JOIN [sys].[dm_exec_requests] tSDER ON
		tSDER.[session_id] = tSDEC.[session_id]
WHERE
	tSDEC.[session_id] NOT IN (@intMySpId)
ORDER BY
	tSDEC.[session_id] ASC
;
