SELECT
	 STR_TO_DATE('01/01/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 100                          AS `EventValue1`
	,RAND() * 100                          AS `EventValue2`

UNION ALL

SELECT
	 STR_TO_DATE('01/02/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 200                          AS `EventValue1`
	,RAND() * 200                          AS `EventValue2`

UNION ALL

SELECT
	 STR_TO_DATE('01/03/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 300                          AS `EventValue1`
	,RAND() * 300                          AS `EventValue2`

UNION ALL

SELECT
	 STR_TO_DATE('01/04/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 400                          AS `EventValue1`
	,RAND() * 400                          AS `EventValue2`

UNION ALL

SELECT
	 STR_TO_DATE('01/05/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 300                          AS `EventValue1`
	,RAND() * 300                          AS `EventValue2`

UNION ALL

SELECT
	 STR_TO_DATE('01/06/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 200                          AS `EventValue1`
	,RAND() * 200                          AS `EventValue2`

UNION ALL

SELECT
	 STR_TO_DATE('01/07/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 100                          AS `EventValue1`
	,RAND() * 100                          AS `EventValue2`
;
