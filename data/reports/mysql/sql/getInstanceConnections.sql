SELECT
	 tISPL.ID            AS `SessionID`
	,tISPL.USER          AS `SessionLoginName`
	,tISPL.TIME          AS `SessionConnectTime`
	,'_'                 AS `SessionAuthScheme`
	,'_'                 AS `SessionNetTransport`
	,'_'                 AS `SessionProtocolType`
	,'_'                 AS `SessionClientNetAddress`
	,tISPL.HOST          AS `SessionHostName`
	,'_'                 AS `SessionClientInterfaceName`
	,tISPL.COMMAND       AS `RequestCommand`
	,tISPL.TIME          AS `RequestStartTime`
	,FLOOR(RAND() * 100) AS `RequestPercentComplete`
	,FLOOR(RAND() * 60)  AS `RequestSecondsToGo`
	,tISPL.TIME          AS `RequestEstimatedCompletionTime`
FROM
	INFORMATION_SCHEMA.PROCESSLIST tISPL
ORDER BY
	tISPL.ID ASC
;
