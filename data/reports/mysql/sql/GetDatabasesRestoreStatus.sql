SELECT
	 1                                     AS `DatabaseId`
	,'db1'                                 AS `DatabaseName`
	,'_'                                   AS `DatabaseStatus`
	,0                                     AS `DatabaseIsInStandby`
	,'_'                                   AS `DatabaseRestoreType`
	,STR_TO_DATE('01/01/2016', '%m/%d/%Y') AS `DatabaseRestoredDateTime`
	,'_'                                   AS `DatabaseRestoredStatus`
	,'server1'                             AS `SourceServerName`
	,'db1_source'                          AS `SourceDatabaseName`
	,STR_TO_DATE('01/01/2016', '%m/%d/%Y') AS `SourceBackupFinishDate`
	,10.25                                 AS `SourceBackupSizeMB`
	,'_'                                   AS `SourceBackupFileName`

UNION ALL

SELECT
	 2                                     AS `DatabaseId`
	,'db2'                                 AS `DatabaseName`
	,'_'                                   AS `DatabaseStatus`
	,0                                     AS `DatabaseIsInStandby`
	,'_'                                   AS `DatabaseRestoreType`
	,STR_TO_DATE('01/02/2016', '%m/%d/%Y') AS `DatabaseRestoredDateTime`
	,'_'                                   AS `DatabaseRestoredStatus`
	,'server2'                             AS `SourceServerName`
	,'db2_source'                          AS `SourceDatabaseName`
	,STR_TO_DATE('01/02/2016', '%m/%d/%Y') AS `SourceBackupFinishDate`
	,20.56                                 AS `SourceBackupSizeMB`
	,'_'                                   AS `SourceBackupFileName`
;
