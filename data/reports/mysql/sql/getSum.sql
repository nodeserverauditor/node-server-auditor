SELECT
	 @@hostname AS `HostName`
	,1          AS `RecordSet`
	,NOW()      AS `CurrentTime`
	,'1 + 1'    AS `Exercise`
	,1 + 1      AS `Solution`

UNION ALL

SELECT
	 @@hostname AS `HostName`
	,1          AS `RecordSet`
	,NOW()      AS `CurrentTime`
	,'5 * 2'    AS `Exercise`
	,5 * 2      AS `Solution`
;

SELECT
	 @@hostname AS `HostName`
	,2          AS `RecordSet`
	,NOW()      AS `CurrentTime`
	,'2 + 2'    AS `Exercise`
	,2 + 2      AS `Solution`

UNION ALL

SELECT
	 @@hostname AS `HostName`
	,2          AS `RecordSet`
	,NOW()      AS `CurrentTime`
	,'3 * 3'    AS `Exercise`
	,3 * 3      AS `Solution`
;
