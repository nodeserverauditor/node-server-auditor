SELECT
	 '0001'       AS `Name`
	,RAND() * 100 AS `Value`

UNION ALL

SELECT
	 '0002'       AS `Name`
	,RAND() * 100 AS `Value`
;

SELECT
	 '1001'       AS `Name`
	,RAND() * 100 AS `Value`

UNION ALL

SELECT
	 '1002'       AS `Name`
	,RAND() * 100 AS `Value`
;
