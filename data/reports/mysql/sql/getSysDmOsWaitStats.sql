SELECT
	 now() AS `EventTime`
	,'A'   AS `WaitType`
	,10.5  AS `WaitTimeMs`

UNION ALL

SELECT
	 now() AS `EventTime`
	,'B'   AS `WaitType`
	,24.45 AS `WaitTimeMs`
;
