SELECT
	 tIST.table_schema                     AS `DatabaseId`
	,tIST.table_schema                     AS `DatabaseName`
	,'_'                                   AS `DatabaseStatus`
	,'_'                                   AS `DatabaseUpdateability`
	,'0'                                   AS `DatabaseBackupIsSnapshot`
	,'_'                                   AS `DatabaseBackupType`
	,'database backup'                     AS `DatabaseBackupName`
	,STR_TO_DATE('01/01/2016', '%m/%d/%Y') AS `DatabaseBackupDateTime`
	,150 + FLOOR(RAND() * 400)             AS `DatabaseBackupSize`
	,100 + FLOOR(RAND() * 300)             AS `DatabaseCompressedBackupSize`
	,'_'                                   AS `DatabaseBackupDevice`
FROM
	information_schema.tables tIST
GROUP BY
	tIST.table_schema
;
