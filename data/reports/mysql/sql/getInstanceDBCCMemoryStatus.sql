SELECT
	 'VM 1' AS `MemoryManager`
	,100    AS `KB`

UNION ALL

SELECT
	 'VM 2' AS `MemoryManager`
	,500    AS `KB`
;
