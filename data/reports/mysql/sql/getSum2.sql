SELECT
	 @@hostname AS `HostName`
	,1          AS `RecordSet`
	,NOW()      AS `CurrentTime`
	,1 + 1      AS `Solution`
;

SELECT
	 @@hostname AS `HostName`
	,2          AS `RecordSet`
	,NOW()      AS `CurrentTime`
	,2 + 2      AS `Solution`
;

SELECT
	 @@hostname AS `HostName`
	,3          AS `RecordSet`
	,NOW()      AS `CurrentTime`
	,3 + 3      AS `Solution`
;
