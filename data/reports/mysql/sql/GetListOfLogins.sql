SELECT
	 tMU.host                              AS `ServerName`
	,tMU.User                              AS `LoginName`
	,STR_TO_DATE('01/01/2016', '%m/%d/%Y') AS `CreateDateTime`
	,1                                     AS `IsHasAccess`
	,0                                     AS `IsDenyLogin`
	,'_'                                   AS `LoginLanguage`
FROM
	mysql.user tMU
;
