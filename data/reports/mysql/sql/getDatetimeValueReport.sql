SELECT
	 STR_TO_DATE('01/01/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 100                          AS `EventValue`

UNION ALL

SELECT
	 STR_TO_DATE('01/02/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 200                          AS `EventValue`

UNION ALL

SELECT
	 STR_TO_DATE('01/03/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 300                          AS `EventValue`

UNION ALL

SELECT
	 STR_TO_DATE('01/04/2016', '%m/%d/%Y') AS `EventDateTime`
	,RAND() * 400                          AS `EventValue`
;
