SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	 @intPageSize   [INTEGER]
	,@DatabaseName  [NVARCHAR](128)
	,@EventDateTime [DATETIME]
	,@strSQLTemp    [NVARCHAR](4000)
;

DECLARE
	@tableDatabases TABLE
	(
		 [DatabaseId]                 [INTEGER]        NOT NULL
		,[SourceDatabaseId]           [INTEGER]        NULL
		,[DatabaseName]               [NVARCHAR](128)  NOT NULL
		,[bigintDatabaseSize]         [DECIMAL](24, 2) NULL
		,[bigintDatabaseDataSize]     [DECIMAL](24, 2) NULL
		,[bigintDatabaseDataSizeUsed] [DECIMAL](24, 2) NULL
		,[bigintDatabaseLogSize]      [DECIMAL](24, 2) NULL
		,[bigintDatabaseLogSizeUsed]  [DECIMAL](24, 2) NULL
	);

CREATE TABLE #tblDatabaseSize
(
	 [bigintDatabaseDataSizeUsed] [DECIMAL](24, 2) NULL
	,[bigintDatabaseLogSizeUsed]  [DECIMAL](24, 2) NULL
);

SET @EventDateTime = GETDATE();

SELECT
	@intPageSize = tSV.[low]
FROM
	[master].[dbo].[spt_values] tSV
WHERE
	tSV.[number] = 1
	AND tSV.[type] = N'E';

INSERT INTO @tableDatabases
(
	 [DatabaseId]
	,[SourceDatabaseId]
	,[DatabaseName]
	,[bigintDatabaseSize]
	,[bigintDatabaseDataSize]
	,[bigintDatabaseLogSize]
)
SELECT
	 tSD.[database_id]
	,tSD.[source_database_id]
	,tSD.[name]
	,SUM(
		CASE
			WHEN (tMF.[is_sparse] = 0) THEN
				CONVERT([DECIMAL](24, 2), tMF.[size])
			ELSE
				CONVERT([DECIMAL](24, 2), divfs.[size_on_disk_bytes])
		END
	)
	,SUM(CASE
		WHEN (tMF.[type_desc] = N'ROWS') THEN
			CASE
				WHEN (tMF.[is_sparse] = 0) THEN
					CONVERT([DECIMAL](24, 2), tMF.[size])
				ELSE
					CONVERT([DECIMAL](24, 2), divfs.[size_on_disk_bytes])
			END
		ELSE
			CONVERT([DECIMAL](24, 2), 0.0)
		END
	)
	,SUM(CASE
		WHEN (tMF.[type_desc] = N'LOG') THEN
			CASE
				WHEN (tMF.[is_sparse] = 0) THEN
					CONVERT([DECIMAL](24, 2), tMF.[size])
				ELSE
					CONVERT([DECIMAL](24, 2), divfs.[size_on_disk_bytes])
			END
		ELSE
			CONVERT([DECIMAL](24, 2), 0.0)
		END
	)
FROM
	[master].[sys].[master_files] tMF
	INNER JOIN [master].[sys].[databases] tSD ON
		tSD.[database_id] = tMF.[database_id]
	LEFT OUTER JOIN [sys].[dm_io_virtual_file_stats](null, NULL) divfs ON
		divfs.[database_id] = tMF.[database_id]
		AND divfs.[file_id] = tMF.[file_id]
GROUP BY
	 tSD.[database_id]
	,tSD.[source_database_id]
	,tSD.[name];

SELECT
	@DatabaseName = MIN(tDB.[DatabaseName])
FROM
	@tableDatabases tDB
WHERE
	tDB.[SourceDatabaseId] IS NULL;

WHILE (@DatabaseName IS NOT NULL)
BEGIN
	IF (DatabasePropertyEx(@DatabaseName, N'Status') IN (N'ONLINE'))
	BEGIN
		SET @strSQLTemp = N'USE ' + QuoteName(@DatabaseName, ']') + N';
			SELECT
				SUM(CASE
					WHEN (tDF.[type_desc] = N''ROWS'') THEN
						CONVERT([DECIMAL](24, 2), FILEPROPERTY(tDF.[name], N''SpaceUsed''))
					ELSE
						CONVERT([DECIMAL](24, 2), 0.0)
				END)
				,SUM(CASE
					WHEN (tDF.[type_desc] = N''LOG'') THEN
						CONVERT([DECIMAL](24, 2), FILEPROPERTY(tDF.[name], N''SpaceUsed''))
					ELSE
						CONVERT([DECIMAL](24, 2), 0.0)
				END)
			FROM
				[sys].[database_files] tDF;
		';

		TRUNCATE TABLE #tblDatabaseSize;

		BEGIN TRY
			INSERT INTO #tblDatabaseSize
			(
				 [bigintDatabaseDataSizeUsed]
				,[bigintDatabaseLogSizeUsed]
			)
				EXEC (@strSQLTemp);
		END TRY
		BEGIN CATCH
			INSERT INTO #tblDatabaseSize
			(
				 [bigintDatabaseDataSizeUsed]
				,[bigintDatabaseLogSizeUsed]
			)
			VALUES
			(
				 -128
				,-128
			);
		END CATCH

		UPDATE @tableDatabases SET
			 [bigintDatabaseDataSizeUsed] = tDBS.[bigintDatabaseDataSizeUsed]
			,[bigintDatabaseLogSizeUsed]  = tDBS.[bigintDatabaseLogSizeUsed]
		FROM
			@tableDatabases tDB
			INNER JOIN #tblDatabaseSize tDBS ON
				tDB.[DatabaseName] = @DatabaseName;
	END

	SELECT
		@DatabaseName = MIN(tDB.[DatabaseName])
	FROM
		@tableDatabases tDB
	WHERE
		tDB.[SourceDatabaseId] IS NULL
		AND tDB.[DatabaseName] > @DatabaseName;
END

SELECT
	 @EventDateTime                                                                                                              AS [EventDateTime]
	,@@ServerName                                                                                                                AS [ServerName]
	,tDB.[DatabaseId]                                                                                                            AS [DatabaseId]
	,tDB.[DatabaseName]                                                                                                          AS [DatabaseName]
	,(tDB.[bigintDatabaseSize]         * CONVERT([DECIMAL](24, 2), @intPageSize)) / (CONVERT([DECIMAL](24, 2), 1024.0 * 1024.0)) AS [DatabaseSizeMB]
	,(tDB.[bigintDatabaseDataSize]     * CONVERT([DECIMAL](24, 2), @intPageSize)) / (CONVERT([DECIMAL](24, 2), 1024.0 * 1024.0)) AS [DatabaseDataSizeMB]
	,(tDB.[bigintDatabaseDataSizeUsed] * CONVERT([DECIMAL](24, 2), @intPageSize)) / (CONVERT([DECIMAL](24, 2), 1024.0 * 1024.0)) AS [DatabaseDataSizeUsedMB]
	,(tDB.[bigintDatabaseLogSize]      * CONVERT([DECIMAL](24, 2), @intPageSize)) / (CONVERT([DECIMAL](24, 2), 1024.0 * 1024.0)) AS [DatabaseLogSizeMB]
	,(tDB.[bigintDatabaseLogSizeUsed]  * CONVERT([DECIMAL](24, 2), @intPageSize)) / (CONVERT([DECIMAL](24, 2), 1024.0 * 1024.0)) AS [DatabaseLogSizeUsedMB]
FROM
	@tableDatabases tDB
ORDER BY
	tDB.[DatabaseName] ASC
;
