SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 getdate()                                           AS [EventDateTime]
	,@@ServerName                                        AS [ServerName]
	,UPPER(SUBSTRING(LTRIM(tSMF.[physical_name]), 1, 1)) AS [Disk]
	,tSD.[database_id]                                   AS [DatabaseId]
	,tSD.[name]                                          AS [DatabaseName]
	,SUM(ISNULL(divfs.[num_of_reads], 0.0))              AS [num_of_reads]
	,SUM(ISNULL(divfs.[num_of_bytes_read], 0.0))         AS [num_of_bytes_read]
	,SUM(ISNULL(divfs.[io_stall_read_ms], 0.0))          AS [io_stall_read_ms]
	,SUM(ISNULL(divfs.[num_of_writes], 0.0))             AS [num_of_writes]
	,SUM(ISNULL(divfs.[num_of_bytes_written], 0.0))      AS [num_of_bytes_written]
	,SUM(ISNULL(divfs.[io_stall_write_ms], 0.0))         AS [io_stall_write_ms]
	,SUM(ISNULL(divfs.[io_stall], 0.0))                  AS [io_stall]
FROM
	[sys].[dm_io_virtual_file_stats](NULL, NULL) AS divfs
	INNER JOIN [master].[sys].[master_files] tSMF ON
		tSMF.[database_id] = divfs.[database_id]
		AND tSMF.[file_id] = divfs.[file_id]
	INNER JOIN [master].[sys].[databases] tSD ON
		tSD.[database_id] = divfs.[database_id]
WHERE
	tSMF.[database_id] IS NOT NULL
	AND tSMF.[physical_name] IS NOT NULL
	AND tSD.[database_id] IS NOT NULL
	AND tSD.[name] IS NOT NULL
GROUP BY
	 UPPER(SUBSTRING(LTRIM(tSMF.[physical_name]), 1, 1))
	,tSD.[database_id]
	,tSD.[name]
;
