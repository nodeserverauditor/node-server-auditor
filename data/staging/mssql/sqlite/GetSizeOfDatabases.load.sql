SELECT
	 dIDS.[EventDateTime]           AS [EventDateTime]
	,SUM(dIDS.[DatabaseDataSizeMB]) AS [DatabaseDataSizeMB]
	,SUM(dIDS.[DatabaseLogSizeMB])  AS [DatabaseLogSizeMB]
FROM
	[dwh_InstanceDatabaseSize] dIDS
WHERE
	dIDS.[InstanceConnectionId] = $_id_connection
GROUP BY
	dIDS.[EventDateTime]
ORDER BY
	dIDS.[EventDateTime] DESC
;
