SELECT
	 now()                                                                               AS `EventDateTime`
	,@@hostname                                                                          AS `ServerName`
	,tIST.table_schema                                                                   AS `DatabaseId`
	,tIST.table_schema                                                                   AS `DatabaseName`
	,SUM(IFNULL(tIST.data_length, 0.0) + IFNULL(tIST.index_length, 0.0)) / (1024 * 1024) AS `DatabaseSizeMB`
	,SUM(IFNULL(tIST.data_length, 0.0)) / (1024 * 1024)                                  AS `DatabaseDataSizeMB`
	,SUM(IFNULL(tIST.data_length, 0.0) - IFNULL(tIST.data_free, 0.0)) / (1024 * 1024)    AS `DatabaseDataSizeUsedMB`
	,0.0                                                                                 AS `DatabaseLogSizeMB`
	,0.0                                                                                 AS `DatabaseLogSizeUsedMB`
FROM
	information_schema.tables tIST
GROUP BY
	tIST.table_schema
;
