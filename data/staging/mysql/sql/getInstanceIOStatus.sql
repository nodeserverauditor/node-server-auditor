SELECT
	 now()        AS `EventDateTime`
	,@@hostname   AS `ServerName`
	,'_'          AS `Disk`
	,0            AS `DatabaseId`
	,'_'          AS `DatabaseName`
	,RAND() * 100 AS `num_of_reads`
	,RAND() * 100 AS `num_of_bytes_read`
	,RAND() * 100 AS `io_stall_read_ms`
	,RAND() * 100 AS `num_of_writes`
	,RAND() * 100 AS `num_of_bytes_written`
	,RAND() * 100 AS `io_stall_write_ms`
	,RAND() * 100 AS `io_stall`
;
