SELECT
	 now()               AS `EventDateTime`
	,FLOOR(RAND() * 100) AS `NumberOfSystemProcesses`
	,FLOOR(RAND() * 100) AS `NumberOfUserProcesses`
	,FLOOR(RAND() * 100) AS `NumberOfActiveUserProcesses`
	,FLOOR(RAND() * 100) AS `NumberOfBlockedProcesses`
;
