INSERT INTO [dwh_InstanceDatabase]
(
	 [EventDateTime]
	,[InstanceConnectionId]
	,[DatabaseId]
	,[DatabaseName]
)
SELECT
	 MAX(t.[EventDateTime])
	,t.[_id_connection]
	,t.[DatabaseId]
	,t.[DatabaseName]
FROM
	[${GetSizeOfDatabases}{0}$] t
	LEFT OUTER JOIN [dwh_InstanceDatabase] dID ON
		dID.[InstanceConnectionId] = t.[_id_connection]
		AND dID.[DatabaseId] = t.[DatabaseId]
		AND dID.[DatabaseName] = t.[DatabaseName]
WHERE
	t.[_id_connection] = $_id_connection
	AND dID.[InstanceConnectionId] IS NULL
GROUP BY
	 t.[_id_connection]
	,t.[DatabaseId]
	,t.[DatabaseName]
;
