SELECT
	 t.[EventDateTime]                AS [EventDateTime]
	,t.[ServerName]                   AS [ServerName]
	,t.[RecordTimeStamp]              AS [RecordTimeStamp]
	,t.[RecordRingBufferType]         AS [RecordRingBufferType]
	,t.[MemoryUtilization]            AS [MemoryUtilization]
	,t.[SystemIdle]                   AS [SystemIdle]
	,t.[SQLProcessCPUUtilization]     AS [SQLProcessCPUUtilization]
	,t.[OtherProcessesCPUUtilization] AS [OtherProcessesCPUUtilization]
FROM
	[${getInstanceCPUUtilization}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[${getInstanceCPUUtilization}{0}$] t
		WHERE
			t.[_id_connection] = $_id_connection
	)
ORDER BY
	 t.[EventDateTime] DESC
	,t.[ServerName] ASC
;
