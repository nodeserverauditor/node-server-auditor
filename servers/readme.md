# Server configuration directory
name your configuration files in format: <serverName>.json
store your configuration in JSON

## mysql connection on remote server example

```json
{
	"type": "mysql",

	"connection": {
		"host":               "Server Name with MySQL instance installed",
		"port":               3306,
		"user":               "MySQL User Name",
		"password":           "My Secret password",
		"database":           "",
		"multipleStatements": true
	},

	"groups": [
		"mysql:dev"
	]
}
```

## mysql connection on local server example

```json
{
	"type": "mysql",

	"connection": {
		"host":               "localhost",
		"socketPath":         "/var/run/mysqld/mysqld.sock",
		"user":               "MySQL User Name",
		"password":           "My Secret password",
		"database":           "",
		"multipleStatements": true
	},

	"groups": [
		"mysql:dev"
	]
}
```

## mssql connection example with sql authentication

```json
{
	"type": "mssql",

	"connection": {
		"domain":    "",
		"user":      "My Valid MSSQL User Name",
		"password":  "My Secret password",
		"server":    "My Server Name with MSSQL installed",
		"database":  "master",
		"driver":    "tedious",
		"stream":    false,
		"parseJSON": false,

		"options":   {
			"instanceName":                     "My MSSQL Instance Name or empty string for default instance",
			"encrypt":                          false,
			"connectTimeout":                   15000,
			"requestTimeout":                   15000,
			"tdsVersion":                       "7_4",
			"rowCollectionOnDone":              false,
			"rowCollectionOnRequestCompletion": false,
			"useColumnNames":                   true,
			"appName":                          "node-mssql",
			"textsize":                         "2147483647",
			"cancelTimeout":                    5000,
			"packetSize":                       4096,
			"isolationLevel":                   2,
			"cryptoCredentialsDetails":         {},
			"useUTC":                           false,
			"connectionIsolationLevel":         2,
			"readOnlyIntent":                   true,
			"enableAnsiNullDefault":            true
		}
	},

	"groups": [
		"mssql:prod"
	]
}
```

## mssql connection example with windows authentication

```json
{
	"type": "mssql",

	"connection": {
		"domain":    "My Windows domain",
		"user":      "My Windows account without domain name",
		"password":  "My Secret Windows password",
		"server":    "My Server Name with MSSQL installed",
		"database":  "master",
		"driver":    "tedious",
		"stream":    false,
		"parseJSON": false,

		"options":   {
			"instanceName":                     "My MSSQL Instance Name or empty string for default instance",
			"encrypt":                          false,
			"connectTimeout":                   15000,
			"requestTimeout":                   15000,
			"tdsVersion":                       "7_1",
			"rowCollectionOnDone":              false,
			"rowCollectionOnRequestCompletion": false,
			"useColumnNames":                   true,
			"appName":                          "node-mssql",
			"textsize":                         "2147483647",
			"cancelTimeout":                    5000,
			"packetSize":                       4096,
			"isolationLevel":                   2,
			"cryptoCredentialsDetails":         {},
			"useUTC":                           false,
			"connectionIsolationLevel":         2,
			"readOnlyIntent":                   true,
			"enableAnsiNullDefault":            true
		}
	},

	"groups": [
		"mssql:prod"
	]
}
```

## Azure connection example

```json
{
	"type": "mssql",

	"connection": {
		"domain":    "",
		"user":      "test@nodeserverauditor",
		"password":  "My Secret Password",
		"server":    "nodeserverauditor.database.windows.net",
		"database":  "NodeServerAuditor",
		"driver":    "tedious",
		"stream":    false,
		"parseJSON": false,

		"options":   {
			"instanceName":                     "",
			"encrypt":                          true,
			"connectTimeout":                   15000,
			"requestTimeout":                   15000,
			"tdsVersion":                       "7_4",
			"rowCollectionOnDone":              false,
			"rowCollectionOnRequestCompletion": false,
			"useColumnNames":                   true,
			"appName":                          "node-mssql",
			"textsize":                         "2147483647",
			"cancelTimeout":                    5000,
			"packetSize":                       4096,
			"isolationLevel":                   2,
			"cryptoCredentialsDetails":         {},
			"useUTC":                           false,
			"connectionIsolationLevel":         2,
			"readOnlyIntent":                   true,
			"enableAnsiNullDefault":            true
		}
	},

	"groups": [
		"mssql:azure"
	]
}
```
