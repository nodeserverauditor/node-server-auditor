'use strict';

(function( module ) {
	var Connector = {
		mssql: require('./type/mssql'),
		mysql: require('./type/mysql')
	};

	module.exports = Connector;

} )( module );
