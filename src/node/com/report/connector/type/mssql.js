'use strict';

(function( module ) {
	var Q             = require( 'q' );
	var util          = require( 'util' );
	var sql           = require( 'mssql' );
	var ConnectorType = require( './' );
	var logger        = require( 'src/node/log' );

	var MS_SQL = function() {
		MS_SQL.super_.apply( this, arguments );

		sql.on( 'error', function( err ) {
			logger.error( '[14]err: {', err, '}' );
			logger.error( '[15]mssql driver error:apply' );
		} );
	};

	util.inherits( MS_SQL, ConnectorType );

	MS_SQL.prototype.query = function( query ) {
		var dfd = Q.defer();

		sql.connect( this.config, function( err ) {
			var request = new sql.Request();

			if ( err ) {
				logger.error( '[30]', err );
				logger.error( '[31]mssql driver error:connect' );
				// logger.debug( '[32]query: {', query, '}' );

				dfd.reject( err );

				return;
			}

			// set multiple recordsets in queries to 'true'
			request.multiple = true;

			request.query( query, function( err, recordset ) {
				if ( err ) {
					logger.error( '[44]', err );
					logger.error( '[45]mssql driver error:query' );
					// logger.debug( '[46]query: {', query, '}' );

					dfd.reject( err );
				} else {
					dfd.resolve( recordset );
				}

				sql.close();
			} );
		} ).on( 'error', function() {
			logger.error( '[57]mssql driver error:socket connection' );
			// logger.debug( '[58]query: {', query, '}' );

			this.close();

			dfd.reject( {
				error: 'mssql socket connection error'
			} );
		} );

		return dfd.promise;
	};

	module.exports = MS_SQL;
} )( module );
