'use strict';

(function( module ) {
	var ConnectorType = function (configuration) {
		this.config = configuration;
	};

	/**
	 * Abstract function to get query
	 *
	 * @returns {object Q.Deferred}
	 */
	ConnectorType.prototype.query = function (query) {
	};

	module.exports = ConnectorType;
} )( module );
