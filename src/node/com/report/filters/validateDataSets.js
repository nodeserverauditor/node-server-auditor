'use strict';

(function( module ) {
	var validateDataSets = function (tables) {
		if ( tables.length && !Array.isArray(tables[0]) ) {
			tables = [tables];
		}

		return tables;
	};

	module.exports = validateDataSets;
} )( module );
