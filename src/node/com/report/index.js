'use strict';

/**
 * Factory module for reportManager instances
 */
(function( module ) {
	var ReportManager          = require('./reportManager');
	var instanceTTL            = 24 * 60 * 60 * 1000; // hh * min * sec * ms
	var TempInstanceHolder     = require('src/node/com/data/model/tempInstanceHolder');
	var reportManagerInstances = {};

	/**
	 * Factory for reportManager instances
	 *
	 * @param {string} sessionId
	 * @returns {ReportManager} report manager instance
	 */
	var reportFactory = function (sessionId) {
		if (!(sessionId in reportManagerInstances)) {
			reportManagerInstances[sessionId] = new TempInstanceHolder(new ReportManager(), instanceTTL);
		}

		return reportManagerInstances[sessionId].get('instance');
	};

	/**
	 * Check for actual instances once in instanceTTL time
	 * @returns {undefined}
	 */
	setInterval( function() {
			for ( var key in reportManagerInstances ) {
				if ( reportManagerInstances.hasOwnProperty(key) && !reportManagerInstances[key].actual() ) {
					delete reportManagerInstances[key];
				}
			}
		},
		instanceTTL
	);

	module.exports = reportFactory;
} )( module );
