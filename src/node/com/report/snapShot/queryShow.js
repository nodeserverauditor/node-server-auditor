'use strict';

(function( module ) {
	var modActs = require('./modelActions');

	module.exports = function (database, params, queryShow) {
		params = params || {};

		var tableNameRe = new RegExp('\\$\\{' + params.name + '\\}\\{(\\d+)\\}\\$', 'g');
		var topName     = params.module.get('type') + params.module.get('name') + params.name;
		var postfix     = 's';
		var queryShowRe = queryShow.replace(tableNameRe, topName + '$1' + postfix);

		return modActs.getConnection(params.serverName, true)
			.then(function (connection) {
				return database.query(queryShowRe, {
					bind: {
						_id_connection: connection.get( 'id_connection' )
					},
					type: database.QueryTypes.SELECT
				});
			})
			.then(function (table) {
				// SQLite3 storage always returns only a single recordset
				return table.length > 0 ? [ table ] : [];
			});
	};
} )( module );
