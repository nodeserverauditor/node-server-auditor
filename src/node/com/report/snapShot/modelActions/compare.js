'use strict';

(function( module ) {
	module.exports = function (one, two) {
		var keys1 = Object.keys(one.attributes).sort();
		var keys2 = Object.keys(two.attributes).sort();

		return (keys1.length === keys2.length) && keys1.every(function (element, index) {
			return element === keys2[index];
		});
	};
} )( module );
