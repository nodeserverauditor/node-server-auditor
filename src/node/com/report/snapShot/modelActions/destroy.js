'use strict';

(function( module ) {
	var Q = require('q');

	var destroy = function (model, params) {
		var dfd = Q.defer();

		params = params || {};

		if ( params.isAll ) {
			model.drop()
				.finally(dfd.resolve);
		} else {
			model.destroy( { where: {
				_id_connection: params.connection.get( 'id_connection' )
			} } )
				.finally(dfd.resolve);
		}

		return dfd.promise;
	};

	module.exports = destroy;
} )( module );
