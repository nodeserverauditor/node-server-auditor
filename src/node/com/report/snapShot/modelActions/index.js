'use strict';

(function( module ) {
	var modelActions = {
		getConnection: require('./getConnection'),
		compare:       require('./compare'),
		destroy:       require('./destroy')
	};

	module.exports = modelActions;
} )( module );
