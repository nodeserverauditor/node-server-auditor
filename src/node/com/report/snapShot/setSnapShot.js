'use strict';

(function( module ) {
	var Q                  = require( 'q');
	var async              = require( 'async' );
	var extend             = require( 'extend' );
	var md5                = require( 'md5' );
	var logger             = require( 'src/node/log' );
	var getModelFromDB     = require( './getModelFromDB' );
	var getModelFromRecord = require( './getModelFromRecord' );
	var modActs            = require( './modelActions' );

	function generateStrHash(columns, record) {
		return md5(JSON.stringify(columns.map(function (colName) {
			return record[colName];
		})));
	}

	module.exports = function( database, params, datasets ) {
		params = params || {};

		var dfd     = Q.defer();
		var topName = params.module.get( 'type' ) + params.module.get( 'name' ) + params.name;

		database.sync().then( function() {
			return modActs.getConnection( params.serverName, false );
		}).then( function( connection ) {
			datasets.forEach( function( datasetSrc, i ) {
				var name          = topName + i;
				var serial        = 1;
				var datasetHashes = {};

				var dataset = datasetSrc.map(function( record ) {
					var targetRec = {};

					extend( true, targetRec, record, {
						_id_connection: connection.get( 'id_connection' )
					} );

					if ( params.primaryKeys ) {
						targetRec._int_order = serial++;

						var recHash = generateStrHash( params.primaryKeys, record );

						if ( recHash in datasetHashes ) {
							logger.error( '[46]dataset for the table [', tname, '] contains rows with duplicated hashes' );

							// should be caught with dfd.reject
							throw new Error('Dataset contains rows with duplicated hashes');
						}

						targetRec[ '_hash' ] = recHash;

						datasetHashes[ recHash ] = targetRec;
					}

					return targetRec;
				} );

				getModelFromDB( database, name, params.primaryKeys ).catch(function( err ) {
					logger.error( '[61]', err );
					logger.error( '[62]database: {', database, '}' );
					logger.error( '[63]name: {', name, '}' );
					logger.error( '[64]params.primaryKeys: {', params.primaryKeys, '}' );

					return null;
				} )
				.then( function( ModelDB ) {
					getModelFromRecord(database, name, datasetSrc, params.primaryKeys).then(function( recordModel ) {
						var updateDfd = Q(true);
						var prDrop    = false;

						if ( !params.notDrop ) {
							if ( 'notDrop' in params ) {
								prDrop = true;
							}

							params.notDrop = false;
						}

						if ( ModelDB ) {
							if ( !modActs.compare(ModelDB, recordModel) ) {
								if ( !params.notDrop ) {
									updateDfd = modActs.destroy(ModelDB, {isAll: true});
								}
							} else {
								if ( !params.primaryKeys ) {
									if ( !params.saveHistory ) {
										updateDfd = modActs.destroy(recordModel, {connection: connection});
									}
								} else {
									// drop, if param notDrop not set
									if ( !params.notDrop && prDrop && !params.saveHistory ) {
										updateDfd = modActs.destroy( ModelDB, {connection: connection} );
									}

									// 'dataset' and its records would be modified within 'mergeDataset'
									updateDfd = mergeDatasetIntoDB(recordModel, connection, params.primaryKeys, params.saveHistory, dataset, datasetHashes);
								}
							}
						}

						updateDfd.finally( function() {
							database.sync().then( function() {
								var promises = dataset.map( function(record) {
									return function( next ) {
										recordModel.create(record).finally(next);
									};
								});

								async.auto(promises, function( err, results ) {
									if ( err ) {
										dfd.resolve();
									} else {
										dfd.reject();
									}
								});
							});
						});
					}, dfd.reject);

					saveRecords( topName, params.module.get('type'), params.module.get('name'), i, database.config.database );
				});
			});
		}).catch(dfd.reject);

		function saveRecords( tablename, connectionType, module, recordset, storage_name ) {
			var defaultDb        = require( 'src/node/database' )();
			var tname            = tablename + recordset;
			var connectionTypeId = 0;
			var idModule         = 0;
			var queryId          = 0;

			defaultDb.models.t_query_result_table.findAll( {
				where: {
					result_table_name: tname
				}
			} ).then( function( data ) { // on success
				if (data && data.length > 0) {
					// logger.debug( '[140]table already exists: {', tname, '}' );
					// logger.debug( '[141]tablename: {', tablename, '}' );
					// logger.debug( '[142]connectionType: {', connectionType, '}' );
					// logger.debug( '[143]module: {', module, '}' );
					// logger.debug( '[144]recordset: {', recordset, '}' );

					return Q.reject( 'table already exists' );
				}

				return defaultDb.models.t_module.findAll( { where: {
					module_name: module + '.' + connectionType
				} } );
			}, function( msg ) { // on reject
				// logger.info( '[153]', msg );
			}).then( function( data ) { // on success
				if ( !data || data.length != 1 ) {
					logger.error( '[156]setSnapShot.js: module do not exists or exists multiple instances' );
					logger.error( '[157]tablename: {', tablename, '}' );
					logger.error( '[158]connectionType: {', connectionType, '}' );
					logger.error( '[159]module: {', module, '}' );
					logger.error( '[160]recordset: {', recordset, '}' );

					if ( data ) {
						logger.error( '[163]data.length: {', data.length, '}' );
					}

					return Q.reject( 'Module do not exists or exists multiple instances' );
				}

				connectionTypeId = data[0].id_connection_type;
				idModule         = data[0].id_module;

				return defaultDb.models.t_report.findOne( {
					where: {
						report_name: params.name,
						id_module:   idModule
					}
				} );
			})
			.then( function( report ) {
				return defaultDb.models.t_query.findOne( {
					where: {
						id_report: report.id_report,
					}
				} );
			} )
			.then( function( qry ) {
					if ( !qry ) {
						logger.error( '[188]No such Query' );

						Q.reject( 'No such Query' );
					}

					return defaultDb.models.t_query_result_database.findOne( {
						where: {
							result_database_name: storage_name
						}
					} ).then( function( rDbu ) {
						var id_result_database = ( rDbu ) ? rDbu.id_result_database : 0;

						if ( id_result_database == 0 ) {
							logger.error( '[201]database :{', storage_name, '} is not exists' );

							return Q.reject( 'No such database name!' );
						}

						return defaultDb.models.t_query_result_table.create( {
							id_recordset:       recordset,
							result_table_name:  tname,
							t_module_id:        idModule,
							id_connection_type: connectionTypeId,
							id_query:           qry.id_query,
							id_result_database: id_result_database
						} );
					} );
				}, function( msg ) { // on reject
					// logger.info( '[216]', msg );
				}
			).then( function() {
				// logger.info( '[219]successfully recorded' );
			}, function( err ) {
				logger.error( '[221]', err );
				logger.error( '[222]tablename: {', tablename, '}' );
				logger.error( '[223]connectionType: {', connectionType, '}' );
				logger.error( '[224]module: {', module, '}' );
				logger.error( '[225]recordset: {', recordset, '}' );
			} );
		}

		return dfd.promise;
	};

	function mergeDatasetIntoDB( Model, connection, primaryKeys, saveHistory, dataset, datasetHashes ) {
		var allKeysAndHash = primaryKeys.concat( [ '_id_connection', '_hash' ] );

		// we have to fetch all the primary keys for record if we wish to update it later on
		return Model.findAll( {
			attributes: allKeysAndHash,
			where: {
				_id_connection: connection.get( 'id_connection' )
			}
		} ).then(function( result ) {
			var promises = [];

			result.forEach(function( row ) {
				var rowHash = row.get('_hash');

				if (rowHash in datasetHashes) {
					var record = datasetHashes[rowHash];

					delete datasetHashes[rowHash];

					allKeysAndHash.forEach(function( colName ) {
						delete record[colName];
					} );

					promises.push(function( next ) {
						row.update(record).finally(next);
					} );
				} else {
					if ( !saveHistory ) {
						promises.push(function (next) {
							row.destroy().finally(next);
						});
					}
				}
			});

			dataset.length = 0;

			for ( var recHash in datasetHashes ) {
				dataset.push(datasetHashes[recHash]);
			}

			var dfd = Q.defer();

			async.auto(promises, function( err, results ) {
				if ( err ) {
					logger.error( '[278]', err );

					dfd.resolve();
				} else {
					dfd.reject();
				}
			} );

			return dfd;
		} );
	}
} )( module );
