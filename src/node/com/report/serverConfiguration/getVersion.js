'use strict';

(function( module ) {
	var fs        = require('fs');
	var path      = require('path');
	var extend    = require('extend');
	var logger    = require('src/node/log');
	var Connector = require('src/node/com/report/connector/');
	var appDir    = path.dirname(require.main.filename);
	var appConfig = require( path.join( appDir, 'configuration.json' ) );

	/**
	 * Get version of SQL server
	 * Use report getver
	 *
	 * @param {object} Server configuration
	 * @param {string} Module name
	 *
	 * @callback {function}
	 */
	module.exports = function( server, type, callback ) {
		var connector  = null;
		var prefs      = {};
		var verRequest = '';
		var verFile    = '';
		var iType      = -1;

		// logger.debug( '[28]getVersion.js:module.exports' );
		// logger.debug( '[29]server: {', server, '}' );
		// logger.debug( '[30]type: {', type, '}' );

		iType = (appConfig.initials.supported[type].order) ? appConfig.initials.supported[type].order : -1;

		// logger.debug( '[34]iType: {', iType, '}' );

		extend( true, prefs, server.get( 'prefs' ) );

		// logger.debug( '[38]prefs: {', prefs, '}' );

		// get from default config
		if ( iType == -1 || !( 'versionFile' in appConfig.initials.supported[type] ) ) {
			// logger.debug( '[42]iType: {', iType, '}' );

			return false;
		}

		verFile = appConfig.initials.supported[type].versionFile;

		// logger.debug( '[49]verFile: {', verFile, '}' );

		verRequest = fs.readFileSync( path.join( appDir, verFile ), 'utf8' );

		// logger.debug( '[53]verRequest: {', verRequest, '}' );

		if ( verRequest === undefined || verRequest == null ) {
			logger.error( '[56]verRequest is undefined' );

			return false;
		}

		connector = new Connector[type]( prefs );

		var cn = connector.query( verRequest ).then( function( resp ) {
			var ret = '';
			var reg = null;

			if ( resp !== undefined ) {
				if ( type == 'mysql' ) {
					ret = resp[0].ServerVersion;
				} else {
					if ( type == 'mssql' ) {
						ret = resp[0][0].ServerVersion;
					} else {
						logger.error( '[74]type is unknown: {', type, '}' );
						logger.error( '[75]resp: {', resp, '}' );
					}
				}

				if ( iType != -1 && 'versionFile' in appConfig.initials.supported[type] ) {
					if( 'versionRegex' in appConfig.initials.supported[type] ) {
						reg = new RegExp( appConfig.initials.supported[type].versionRegex );
						ret = ret.match( reg );

						if ( typeof( ret ) == 'object' ) {
							ret = ret[0];
						} else {
							ret = '0';
						}
					}
				}
			}

			callback( {
				success:   true,
				ver:       ret,
				conn_type: type
			} );
		},

		function( respErr ) {
			logger.error( '[101]respErr: {', respErr, '}' );
			logger.error( '[102]server: {', server, '}' );
			logger.error( '[103]conn_type: {', type, '}' );

			callback( {
				success:   false,
				ver:       '',
				conn_type: type
			} );
		} );
	};
} )( module );
