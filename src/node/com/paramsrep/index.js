'use strict';

(function( module ) {
	var async        = require('async');
	var Q            = require('q');
	var fs           = require('fs');
	var path         = require('path');
	var extend       = require('extend');
	var Promise      = require('promise');
	var Report       = require('src/node/com/report');
	var database     = require('src/node/database');
	var dbparams     = require('src/node/database/model').t_query_parameter;
	var Connector    = require('src/node/com/report/connector/');
	var Filters      = require('src/node/com/report/filters');
	var logger       = require('src/node/log');
	var ParamsUtils  = require('src/node/com/paramsrep/paramsUtils');
	var appDir       = path.dirname(require.main.filename);
	var modulesPath  = path.join(appDir, 'data');

	// Type for cast
	var constTypeNumber = 'Number';

	/**
	 * ParamsRep class
	 *
	 */
	function ParamsRep() {
	}

	/**
	 * function get params select report
	 *
	 * nameRep
	 */
	ParamsRep.prototype.getParamsReport = function(nameRep, sessionId) {
		var report        = null;
		var parameters    = null;
		var prefs         = null;
		var connector     = null;
		var request       = null;
		var typeConnect   = null;
		var pathFile      = null;
		var arr_val       = [];
		var aPromise      = [];
		var arrJson       = null;
		var pathSql       = null;
		var module        = null;
		var server        = null;
		var dfd           = Q.defer();
		var version       = null;
		var rReturnParams = { extract: [], transformation: [], load: [] };

		if ( !Report(sessionId).isConfigured() ) {
			logger.error( '[54]session is not configured' );

			dfd.reject( {
				error: 'session is not configured'
			});

			return dfd.promise;
		}

		module = Report(sessionId).module();
		server = Report(sessionId).getServer();

		var serverName = server.get('name');

		if ( !module ) {
			logger.error( '[69]index.js: report module is not defined' );

			dfd.reject( {
				error: 'report module is not defined'
			} );

			return dfd.promise;
		}

		typeConnect = module.get('type');

		if ( !Connector[typeConnect] ) {
			logger.error( '[81]index.js: connector type not exists' );

			dfd.reject( {
				error: 'connector type not exists'
			} );

			return dfd.promise;
		}

		report = module.get('reports').find(nameRep);

		arrJson = module.get('repJson');

		pathSql = findPathReport(arrJson, nameRep);

		if ( pathSql == null || pathSql.length === 0 || !pathSql.trim() ) {
			dfd.reject( {
				error: 'sql path is not found or empty'
			} );

			return dfd.promise;
		}

		if ( !report ) {
			dfd.reject( {
				error: 'report(' + nameRep + ') not found'
			} );

			return dfd.promise;
		}

		version = Report(sessionId).getSavedVersion();

		prefs = {};

		extend(true, prefs, server.get('prefs'));

		pathFile = path.dirname( path.join( modulesPath, pathSql ));

		connector = new Connector[typeConnect](prefs);

		return ParamsUtils.getExtractParameters( report, typeConnect, serverName, nameRep, connector, pathSql )
		.then( function( aExtract ) {
			if ( aExtract ) {
				rReturnParams.extract = aExtract;
			}

			return rReturnParams;
		} )
		.then( function( rParams ) {
			return ParamsUtils.getTransformParameters( report, typeConnect, serverName, nameRep, connector, pathSql )
			.then( function( aTransform ) {
				if ( aTransform ) {
					rParams.transformation = aTransform;
				}

				return rParams;
			} );
		} )
		.then( function( rParams ) {
			return ParamsUtils.getLoadParameters( report, typeConnect, serverName, nameRep, connector, pathSql )
			.then( function( aLoad ) {
				if ( aLoad ) {
					rParams.load = aLoad;
				}

				return rParams;
			} );
		} );
		parameters = report.get('parameters');

		if ( !parameters ) {
			dfd.reject( {
				error: 'Not find property parameters'
			} );

			return dfd.promise;
		} else {
			prefs = {};

			extend(true, prefs, server.get('prefs'));

			pathFile = path.dirname( path.join( modulesPath, pathSql ));

			connector = new Connector[typeConnect](prefs);

			parameters.forEach(function(param) {
				aPromise.push( new Promise( function( resolve, reject ) {
					param.connType = typeConnect;
					param.connName = serverName;
					param.nameRep  = nameRep;

					dbparams.find({
						where: {
							// connType:  param.connType,
							// connName:  param.connName,
							// repName:   param.nameRep,
							// TODO SSK check param report check
							nameParam: param.name
						}
					}

					).then(function (record) {
						// if ( record ) {
						// 	switch( param.show ) {
						// 		case constTypeNumber:
						// 			param.value = parseInt(record.valueParam);
						// 			break;
						// 		default:
						// 			param.value = record.valueParam;
						// 			break;
						// 	}
						// }

						if ( param.query.hasOwnProperty( 'file' ) ) {
							param.query.valquery = [];

							var strQueryFileName = ParamsUtils.getFileNameFromVersion( param.query.file, version );

							try {
								request = fs.readFileSync( path.join( pathFile, strQueryFileName ), 'utf8');
							}
							catch ( err ) {
								logger.error( '[203]', err );
								logger.error( '[204]pathFile: {', pathFile, '}' );
								logger.error( '[205]strQueryFileName: {', strQueryFileName, '}' );

								resolve(param);

								return;
							}

							connector.query(request).then(function (records) {
								records = Filters.delArrayDataSets(records);
								param.query.valquery.push(records);
								resolve(param);
							}, function( err ) {
								logger.error( '[217]', err );

								reject( err );
							});
						} else {
							resolve(param);
						}
					});
				}));
			});

			Promise.all( aPromise ).then( function( resp ) {
				dfd.resolve(resp);
			}, function( err ) {
				logger.error( '[232]', err );

				dfd.reject( err );
			});
		}

		return dfd.promise;

		function findPathReport( arrMenu, nameRep ) {
			var ret = '';

			for ( var i = 0; i < arrMenu.length; i++ ) {
				if ( arrMenu[i].hasOwnProperty('reports') ) {
					for ( var j = 0; j < arrMenu[i].reports.length; j++ ) {
						if ( arrMenu[i].reports[j].name === nameRep ) {
							if ( arrMenu[i].reports[j].hasOwnProperty('pathSql') ) {
								return arrMenu[i].reports[j].pathSql;
							}
						}
					}
				}
			};

			return ret;
		}
	};

	ParamsRep.prototype.save = function(data, sessionId) {
		var dfd = Q.defer();

		dbparams.sync().then(function() {
			var promises = [];

			data.forEach(function (item) {
				promises.push(function (next) {
					var where = {
						// connType:      item.connType,
						// connName:      item.connName,
						// repName:       item.nameRep,
						// TODO SSK check repname, ...
						nameParam:     item.name,
						paragraphName: item.paragraphName || '',
						sectionName:   item.sectionName || ''
					};

					dbparams.find( {
						where: where
					} ).then( function (record) {
						if ( record ) {
							// record.updateAttributes({
							//    valueParam: item.value
							// }).finally(next);
						} else {
							dbparams.create( {
								nameParam:     item.name,
								paragraphName: item.paragraphName || '',
								sectionName:   item.sectionName || ''
								// TODO SSK for id_query, is_active, ,,,
								// connType:      item.connType,
								// connName:      item.connName,
								// repName:       item.nameRep,
								// valueParam:    item.value,
							} ).finally(next);
						}
					});
				});
			});

			async.auto( promises, function( err, results ) {
				if ( err ) {
					logger.error( '[303]', err );

					dfd.reject();
				} else {
					dfd.resolve();
				}
			});
		});

		return dfd.promise;
	};

	/*
	 * Storage for all ParamsReport
	 */
	var paramrep_instances = { };

	/*
	 *
	 */
	module.exports = function ( sessionId ) {
		if ( !( sessionId in paramrep_instances ) ) {
			paramrep_instances[sessionId] = new ParamsRep();
		}

		return paramrep_instances[sessionId];
	};
} )( module );
