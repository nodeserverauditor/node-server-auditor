'use strict';

/**
 * Describes Report module, that contains information about available reports
 *
 * @param {type} module
 */
(function( module ) {
	var util  = require('util');
	var Model = require('./');

	var ReportModule = function( params ) {
		ReportModule.super_.apply(this, arguments);

		this.set('name',        params.name);
		this.set('type',        params.type);
		this.set('description', params.description);
		this.set('reports',     params.reports || []); //[<ReportItem>]
		this.set('repJson',     params.repJson || []);
	};

	util.inherits(ReportModule, Model);

	module.exports = ReportModule;
} )( module );
