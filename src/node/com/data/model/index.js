'use strict';

(function( module ) {
	var Model = function( params ) {
		this.container = {};
	};

	Model.prototype.set = function( key, value ) {
		return this.container[key] = value;
	};

	Model.prototype.get = function (key) {
		return this.container[key];
	};

	Model.prototype.isValid = function() {
		for ( var key in this.container ) {
			if ( this.container.hasOwnProperty( key ) && this.container[key] === undefined ) {
				return false;
			}
		}

		return true;
	};

	Model.prototype.data = function() {
		return recoursive( this.container );

		function recoursive( obj ) {
			var result = {};

			for ( var key in obj ) {
				switch (true) {
					case obj[key] instanceof Model:
						result[key] = obj[key].data();
						break;
					case Object.prototype.toString.call(obj[key]) === '[object Object]':
						result[key] = recoursive(obj[key]);
						break;
					default:
						result[key] = obj[key];
						break;
				}
			}

			return result;
		}
	};

	module.exports = Model;
} )( module );
