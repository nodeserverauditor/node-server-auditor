'use strict';

(function( module ) {
	var util  = require( 'util' );
	var Model = require( './' );

	var ReportResult = function( reportItem, result ) {
		ReportResult.super_.apply( this, arguments );

		var details     = reportItem.get( 'request' )[0];
		var primarykeys = details.report['primary key'];
		var reportExtra = {
			'fileName':           details.report.file[0].file,
			'storage':            details.report.storage,
			'query':              details.query,
			'primarykeys':        primarykeys.join(),
			'savehistoryrecords': details.report.savehistoryrecords
		};

		this.set('name',        reportItem.get('name'));
		this.set('component',   reportItem.get('component'));
		this.set('description', reportItem.get('description'));
		this.set('details',     reportExtra);
		this.set('result',      result);
	};

	util.inherits( ReportResult, Model );

	module.exports = ReportResult;
} )( module );
