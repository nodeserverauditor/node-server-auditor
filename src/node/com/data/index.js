'use strict';

(function( module ) {
	var Data = {};

	Data.ServerConfiguration = require('./model/serverConfiguration');
	Data.DB_Configuration    = require('./model/dbConfiguration');
	Data.ReportItem          = require('./model/reportItem');
	Data.ReportResult        = require('./model/reportResult');
	Data.ReportModule        = require('./model/reportModule');
	Data.JSON_Response       = require('./model/jsonResponse');
	Data.Model               = require('./model');
	Data.Collection          = require('./model/collection');
	Data.ReportCollection    = require('./model/reportCollection');

	module.exports = Data;
} )( module );
