'use strict';

(function( module ) {
	var Q                           = require( 'q' );
	var async                       = require( 'async' );
	var Promise                     = require( 'promise' );
	var _                           = require( 'lodash' );
	var nodeSchedule                = require( 'node-schedule' );
	var Report                      = require( 'src/node/com/report' );
	var logger                      = require( 'src/node/log' );
	var database                    = require( 'src/node/database')();
	var dbsched                     = require( 'src/node/database/model' );
	var t_group                     = require( 'src/node/database/model/t_group' );
	var t_connection                = require( 'src/node/database/model/t_connection' );
	var t_connection_query_schedule = require( 'src/node/database/model/t_connection_query_schedule' );
	var t_connection_type           = require( 'src/node/database/model/t_connection_type' );
	var t_module                    = require( 'src/node/database/model/t_module' );
	var t_report                    = require( 'src/node/database/model/t_report' );

	/**
	 * Report class
	 *
	 */
	function Scheduler() {
		// logger.debug( '[25]Scheduler.prototype.enable' );

		this._arr_sched = [];
	}

	/**
	 * Enable determined task
	 *
	 * @param {mixed} record It may be record Object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.enable = function( record, sessionId ) {
		// logger.debug( '[37]Scheduler.prototype.enable' );
		// logger.debug( '[38]record: {', record, '}' );
		// logger.debug( '[39]sessionId: {', sessionId, '}' );

		var active_schedule = null;
		var recent          = true;
		var dfd             = Q.defer();

		enableByRecord = enableByRecord.bind( this );

		if ( typeof( record ) === 'object' ) {
			// logger.debug( '[48]record: {', record, '}' );

			enableByRecord( record );
		} else {
			// logger.debug( '[52]record: {', record, '}' );

			this.getRecord( record ).then( enableByRecord, dfd.reject);
		}

		return dfd.promise;

		function enableByRecord( record ) {
			// logger.debug( '[60]record: {', record, '}' );

			t_connection_query_schedule.findById(
				record.id_connection_query_schedule, {
					include: [ {
						model: t_report,

						include: [ {
							model: t_module
						} ]
					}, {
						model: t_connection,

						include: [ {
							model: t_connection_type
						} ]
					}, {
						model: t_group,

						include: [ {
							model: t_connection_type
						} ]
					} ]
				} )
			.then( function( connection_query_schedule ) {
				// logger.debug( '[85]connection_query_schedule: {', connection_query_schedule, '}' );

				if ( connection_query_schedule ) {
					var result = {};

					result.id_connection_query_schedule = connection_query_schedule.id_connection_query_schedule;
					result.connType                     = connection_query_schedule.t_connection.t_connection_type.connection_type_name;
					result.connName                     = connection_query_schedule.t_connection.serverName;
					result.moduleType                   = connection_query_schedule.t_report.t_module.module_name;
					result.repName                      = connection_query_schedule.t_report.report_name;
					result.isEnabled                    = connection_query_schedule.isEnabled;
					result.schedule                     = connection_query_schedule.schedule;
					result.id_group                     = connection_query_schedule.id_group;

					// logger.debug( '[99]result.id_connection_query_schedule: {', result.id_connection_query_schedule, '}' );
					// logger.debug( '[100]result.connType: {', result.connType, '}' );
					// logger.debug( '[101]result.connName: {', result.connName, '}' );
					// logger.debug( '[102]result.moduleType: {', result.moduleType, '}' );
					// logger.debug( '[103]result.repName: {', result.repName, '}' );
					// logger.debug( '[104]result.isEnabled: {', result.isEnabled, '}' );
					// logger.debug( '[105]result.schedule: {', result.schedule, '}' );

					var repName    = result.repName;
					var connName   = result.connName;
					var moduleType = result.moduleType;
					var schedule   = result.schedule;

					if ( !record.forGroup ) {
						// logger.debug( '[113]nameJob: {', nameJob, '}' );
						// logger.debug( '[114]schedule: {', schedule, '}' );

						active_schedule = nodeSchedule.scheduleJob( result.id_connection_query_schedule + '', schedule, function() {
							// logger.debug( '[117]repName: {', repName, '}' );
							// logger.debug( '[118]connName: {', connName, '}' );
							// logger.debug( '[119]moduleType: {', moduleType, '}' );
							// logger.debug( '[120]recent: {', recent, '}' );

							Report( sessionId ).getReportByScheduler( repName, connName, moduleType, recent )
								.then( data => {
									return dfd.resolve( data );
								} )
								.catch( err => {
									logger.error( '[127]', err );
									logger.error( '[128]repName: {', repName, '}' );
									logger.error( '[129]connName: {', connName, '}' );
									logger.error( '[130]moduleType: {', moduleType, '}' );
									logger.error( '[131]recent: {', recent, '}' );

									return dfd.reject( err );
								} )
						} );
					} else {
						dbsched.t_connection_group.findAndCountAll( {
							where: {
								id_group: result.id_group
							},
							include: [ {
								model: dbsched.t_connection
							} ]
						} ).then( function( aData ) {
							var aProm = [];

							if ( aData.count ) {
								for ( var i = 0; i < aData.count; i++ ) {
									aProm.push( new Promise( function( resolve, reject ) {
										var connName2 = _.clone( aData.rows[i].dataValues.t_connection.dataValues.serverName );

										active_schedule = nodeSchedule.scheduleJob( result.id_connection_query_schedule + '', schedule, function() {
											Report( sessionId ).getReportByScheduler( repName, connName2, moduleType, recent )
												.then( function( repData ) {
													resolve( repData );
												},
												function( err ) {
													logger.error( '[158]', err );
													logger.error( '[159]repName: {', repName, '}' );
													logger.error( '[160]connName2: {', connName2, '}' );
													logger.error( '[161]moduleType: {', moduleType, '}' );
													logger.error( '[162]recent: {', recent, '}' );

													reject( err );
												} );
										} );
									} ) );
								}

								if ( aProm.length ) {
									return Promise.all( aProm ).then( function() {
										return dfd.resolve();
									} );
								}
							} else {
								return dfd.reject();
							}
						} );
					}

					updateSelectorPtr( result, true )
						.then(dfd.resolve, dfd.reject);
				}
			});
		}
	};

	/**
	 * Disable determined task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.disable = function( record, sessionId ) {
		// logger.debug( '[195]Scheduler.prototype.disable' );
		// logger.debug( '[196]record: {', record, '}' );
		// logger.debug( '[197]sessionId: {', sessionId, '}' );

		var dfd  = Q.defer();
		var jobs = this.getStartedJobs();

		disableByRecord = disableByRecord.bind( this );

		if ( typeof( record ) === 'object' ) {
			// logger.debug( '[205]record: {', record, '}' );

			disableByRecord( record );
		} else {
			// logger.debug( '[209]record: {', record, '}' );

			this.getRecord( record ).then( disableByRecord, dfd.reject );
		}

		return dfd.promise;

		function disableByRecord( record ) {
			t_connection_query_schedule.findById(
				record.id_connection_query_schedule, {
					include: [ {
						model: t_report,

						include: [ {
							model: t_module
						} ]
					}, {
						model: t_connection,

						include: [ {
							model: t_connection_type
						} ]
					}, {
						model: t_group,

						include: [ {
							model: t_connection_type
						} ]
					} ]
				} )
			.then( function( connection_query_schedule ) {
				// logger.debug( '[240]connection_query_schedule: {', connection_query_schedule, '}' );

				if ( connection_query_schedule ) {
					var result = {};

					result.id_connection_query_schedule = connection_query_schedule.id_connection_query_schedule;
					result.connType                     = connection_query_schedule.t_connection.t_connection_type.connection_type_name;
					result.connName                     = connection_query_schedule.t_connection.serverName;
					result.moduleType                   = connection_query_schedule.t_report.t_module.module_name;
					result.repName                      = connection_query_schedule.t_report.report_name;
					result.isEnabled                    = connection_query_schedule.isEnabled;
					result.schedule                     = connection_query_schedule.schedule;

					// logger.debug( '[253]result.id_connection_query_schedule: {', result.id_connection_query_schedule, '}' );
					// logger.debug( '[254]result.connType: {', result.connType, '}' );
					// logger.debug( '[255]result.connName: {', result.connName, '}' );
					// logger.debug( '[256]result.moduleType: {', result.moduleType, '}' );
					// logger.debug( '[257]result.repName: {', result.repName, '}' );
					// logger.debug( '[258]result.isEnabled: {', result.isEnabled, '}' );
					// logger.debug( '[259]result.schedule: {', result.schedule, '}' );

					var currJobName = result.id_connection_query_schedule + '';

					if ( jobs.hasOwnProperty( currJobName ) ) {
						jobs[currJobName].cancel();

						updateSelectorPtr( result, false )
							.then( dfd.resolve, dfd.reject );
					} else {
						dfd.reject( {
							'result': 'job not found'
						} );
					}
				}
			});
		};
	};

	Scheduler.prototype.getStartedJobs = function() {
		// logger.debug( '[279]Scheduler.prototype.getStartedJobs' );

		return nodeSchedule.scheduledJobs;
	};

	Scheduler.prototype.list = function() {
		// logger.debug( '[285]Scheduler.prototype.list' );

		var tables = [];
		var dfd    = Q.defer();

		t_connection_query_schedule.findAll( {
			include: [ {
				model: t_report,

				include: [ {
					model: t_module
				} ]
			}, {
				model: t_connection,

				include: [ {
					model: t_connection_type
				} ]
			}, {
				model: t_group,

				include: [ {
					model: t_connection_type
				} ]
			} ]
		} ).then( function( table ) {
			var result = table.map( extractItem );

			if ( result ) {
				tables.push( result );
			}

			dfd.resolve(tables);
		} )
		.finally(function() {
			dfd.resolve( tables );
		} );

		return dfd.promise;

		function extractItem( item ) {
			var dataValues = item.dataValues;
			var resItem    = {};

			for ( var key in dataValues ) {
				if ( dataValues.hasOwnProperty( key ) ) {
					resItem[key] = dataValues[key];
				}
			}

			resItem.repName    = dataValues.t_report.report_name;
			resItem.moduleType = dataValues.t_report.t_module.module_name;
			resItem.connType   = dataValues.t_connection.t_connection_type.connection_type_name;
			resItem.connName   = dataValues.t_connection.serverName;
			resItem.groupName  = dataValues.t_group.group_name;

			return resItem;
		};
	};

	/**
	 * insert (create) new scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.insert = function( record, sessionId ) {
		// logger.debug( '[352]Scheduler.prototype.insert' );
		// logger.debug( '[353]record: {', record, '}' );
		// logger.debug( '[354]sessionId: {', sessionId, '}' );

		var dfd = Q.defer();

		dbsched.t_connection_query_schedule.sync().then( function() {
			var promises = [];

			promises.push( new Promise( function( resolve, reject ) {
				// logger.debug( '[362]record.connType: {', record.connType, '}' );

				dbsched.t_connection_type.findOrCreate( {
					where: {
						connection_type_name: record.connType
					}
				}).spread( function( TConnectionType, created ) {
					// logger.debug( '[369]TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );

					// logger.debug( '[371]TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
					// logger.debug( '[372]record.connName: {', record.connName, '}' );

					dbsched.t_connection.findOrCreate( {
						where: {
							id_connection_type: TConnectionType.id_connection_type,
							serverName:         record.connName
						}
					}).spread( function( TConnection, created ) {
						// logger.debug( '[380]TConnection.id_connection: {', TConnection.id_connection, '}' );

						// logger.debug( '[382]TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
						// logger.debug( '[383]record.groupName: {', record.groupName, '}' );

						dbsched.t_group.findOrCreate( {
							where: {
								id_connection_type: TConnectionType.id_connection_type,
								group_name:         record.groupName
							}
						}).spread( function( TGroup, created ) {
							// logger.debug( '[391]TGroup.id_group: {', TGroup.id_group, '}' );

							// logger.debug( '[393]TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
							// logger.debug( '[394]record.moduleType: {', record.moduleType, '}' );

							dbsched.t_module.findOrCreate( {
								where: {
									id_connection_type: TConnectionType.id_connection_type,
									module_name:        record.moduleType
								}
							} ).spread( function( TModule, created ) {
								// logger.debug( '[402]TModule.id_module: {', TModule.id_module, '}' );

								// logger.debug( '[404]TModule.id_module: {', TModule.id_module, '}' );
								// logger.debug( '[405]record.repName: {', record.repName, '}' );

								dbsched.t_report.findOrCreate( {
									where: {
										id_module:   TModule.id_module,
										report_name: record.repName
									}
								}).spread( function( TReport, created ) {
									// logger.debug( '[413]report.id_report: {', TReport.id_report, '}' );

									// logger.debug( '[415]TConnection.id_connection: {', TConnection.id_connection, '}' );
									// logger.debug( '[416]TGroup.id_group: {', TGroup.id_group, '}' );
									// logger.debug( '[417]record.forGroup: {', record.forGroup, '}' );
									// logger.debug( '[418]TReport.id_report: {', TReport.id_report, '}' );
									// logger.debug( '[419]record.schedule: {', record.schedule, '}' );
									// logger.debug( '[420]record.isEnabled: {', record.isEnabled, '}' );

									dbsched.t_connection_query_schedule.create( {
										'id_connection': TConnection.id_connection,
										'id_group':      TGroup.id_group,
										'forGroup':      record.forGroup,
										'id_report':     TReport.id_report,
										'schedule':      record.schedule,
										'isEnabled':     record.isEnabled
									} ).then( function( res ) {
										// logger.debug( '[430]res.id_connection_query_schedule: {', res.id_connection_query_schedule, '}' );

										record.id_connection_query_schedule = res.id_connection_query_schedule;

										resolve( record );
									}).catch(function( err ) {
										logger.error( '[436]', err );

										reject( err );
									});
								});
							});
						});
					});
				});
			}));

			Promise.all( promises ).then( function( resp ) {
				dfd.resolve(resp);
			}, function( err ) {
				logger.error( '[450]', err );

				dfd.reject(err);
			});
		});

		return dfd.promise;
	};

	/**
	 * update existing scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.update = function( id_connection_query_schedule, newRecord, sessionId ) {
		// logger.debug( '[466]Scheduler.prototype.update' );
		// logger.debug( '[467]id_connection_query_schedule: {', id_connection_query_schedule, '}' );
		// logger.debug( '[468]newRecord: {', newRecord, '}' );
		// logger.debug( '[469]sessionId: {', sessionId, '}' );

		var enablePtr = this.enable;

		// set 'id_connection_query_schedule'
		newRecord.id_connection_query_schedule = id_connection_query_schedule;

		return this.getRecord( id_connection_query_schedule ).then( function( record ) {
			// logger.debug( '[477]record: {', record, '}' );

			this.disable( record, sessionId )
				.then( function() {
					// logger.debug( '[481]newRecord.connType: {', newRecord.connType, '}' );

					return dbsched.t_connection_type.findOrCreate( {
						where: {
							connection_type_name: newRecord.connType
						}
					} ).spread( function( TConnectionType, created ) {
						// logger.debug( '[488]TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );

						// logger.debug( '[490]TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
						// logger.debug( '[491]newRecord.connName: {', newRecord.connName, '}' );

						dbsched.t_connection.findOrCreate( {
							where: {
								id_connection_type: TConnectionType.id_connection_type,
								serverName:         newRecord.connName
							}
						} ).spread( function( TConnection, created ) {
							// logger.debug( '[499]TConnection.id_connection: {', TConnection.id_connection, '}' );

							// set 'id_connection'
							newRecord.id_connection = TConnection.id_connection;

							// logger.debug( '[504]newRecord.moduleType: {', newRecord.moduleType, '}' );
							// logger.debug( '[505]TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );

							dbsched.t_module.findOrCreate( {
								where: {
									module_name:        newRecord.moduleType,
									id_connection_type: TConnectionType.id_connection_type
								}
							} ).spread( function( TModule, created ) {
								// logger.debug( '[513]TModule.id_module: {', TModule.id_module, '}' );

								// logger.debug( '[515]TModule.id_module: {', TModule.id_module, '}' );
								// logger.debug( '[516]newRecord.repName: {', newRecord.repName, '}' );

								dbsched.t_report.findOrCreate( {
									where: {
										id_module:   TModule.id_module,
										report_name: newRecord.repName
									}
								}).spread( function( TReport, created ) {
									// logger.debug( '[524]TReport.id_report: {', TReport.id_report, '}' );

									// set 'id_report'
									newRecord.id_report = TReport.id_report;

									record.updateAttributes( newRecord )
										.then( function( result ) {
											enablePtr( newRecord, sessionId );
										}.bind( this ));
								} );
							} );
						} );
					} );
				}.bind(this), function() {
					// logger.debug( '[538]newRecord: {', newRecord, '}' );

					record.updateAttributes( newRecord );
				} );
		}.bind( this ));
	};

	/**
	 * delete (remove) existing scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.delete = function( id_connection_query_schedule, sessionId ) {
		// logger.debug( '[552]Scheduler.prototype.delete' );

		var dfd = Q.defer();

		// logger.debug( '[556]Scheduler.prototype.delete' );

		this.getRecord( id_connection_query_schedule ).then(function( record ) {
			this.disable( record, sessionId ).finally( function() {
				dbsched.t_connection_query_schedule.sync().then( function() {
					dbsched.t_connection_query_schedule.destroy( {
						where: {
							id_connection_query_schedule: id_connection_query_schedule
						}
					} ).then( dfd.resolve, dfd.reject );
				}, dfd.reject);
			} );
		}.bind( this ), dfd.reject );

		return dfd.promise;
	};

	/**
	 * update 'enabled' attribute for the existing scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	function updateSelectorPtr( record, selector ) {
		// logger.debug( '[580]updateSelectorPtr' );
		// logger.debug( '[581]record: {', record, '}' );
		// logger.debug( '[582]selector: {', selector, '}' );

		var dfd = Q.defer();

		dbsched.t_connection_query_schedule.sync().then( function() {
			var promises = [];

			promises.push(function( next ) {
				dbsched.t_connection_query_schedule.find( {
					where: {
						id_connection_query_schedule: record.id_connection_query_schedule
					}
				} )
				.then( function ( record ) {
					if ( record ) {
						record.updateAttributes( {
							isEnabled: selector
						} ).catch( function( err ) {
							logger.error( '[600]', err );

							dfd.reject( err );
						} );
					}
				} ).finally( next );
			} );

			async.auto(promises, function( err, results ) {
				if ( err ) {
					logger.error( '[610]', err );

					dfd.reject( err );
				} else {
					dfd.resolve( results );
				}
			});
		});

		return dfd.promise;
	};

	Scheduler.prototype.getRecord = function( id_connection_query_schedule ) {
		// logger.debug( '[623]Scheduler.prototype.getRecord' );

		var dfd = Q.defer();

		dbsched.t_connection_query_schedule.findById( id_connection_query_schedule ).then( function( record ) {
			if ( record ) {
				dfd.resolve( record );
			} else {
				logger.error( '[631]record not found:id_connection_query_schedule: {', id_connection_query_schedule, '}' );

				dfd.reject( 'record not found' );
			}
		}, dfd.reject );

		return dfd.promise;
	};

	/*
	 * a storage for all schedulers
	 */
	var scheduler_instances = { };

	/*
	 * a factory for report instances
	 */
	module.exports = function( sessionId ) {
		// logger.debug( '[649]module.exports' );

		if ( !( sessionId in scheduler_instances ) ) {
			scheduler_instances[sessionId] = new Scheduler();
		}

		return scheduler_instances[sessionId];
	};
} )( module );
