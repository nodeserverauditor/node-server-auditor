'use strict';

(function( module ) {
	var validator    = require( 'validator' );
	var fs           = require( 'fs' );
	var path         = require( 'path' );
	var logger       = require( 'src/node/log' );
	var dictionaries = null;

	module.exports = {
		get: function( lang ) {
			var config = require( 'configuration' );
			var name   = validator.whitelist( lang, 'A-Za-z' );

			name = (name !== 'default')
				? name
				: config.language;

			try {
				return require( path.join( 'i18n', name ) );
			}
			catch ( err ) {
				logger.error( '[23]', err );
				logger.error( '[24]name: {', name, '}' );

				return false;
			}
		},

		list: function() {
			var pattern = /^([\w\-.]+)\.json$/;

			if ( dictionaries === null ) {
				dictionaries = fs.readdirSync( 'i18n/' )
					.filter(function( item ) {
						return pattern.test(item);
					})
					.map(function( item ) {
						return item.replace( pattern, '$1' );
					});
			}

			return dictionaries;
		}
	};
} )( module );
