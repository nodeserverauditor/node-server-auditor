'use strict';

(function( module ) {
	var express   = require('express');
	var scheduler = require('src/node/com/scheduler');
	var logger    = require('src/node/log');
	var router    = express.Router();
	var handler   = null;

	/*
	 * get all scheduled tasks
	 */
	router.get('/', function (req, res) {
		scheduler(req.sessionId).list().then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			logger.error( '[17]', err );
			logger.error( '[18]error by getting the list of scheduled tasks' );

			res.json(handler.error('Error getting schedule list', err));
		});
	});

	/*
	 * add new scheduled task
	 */
	router.post('/', function (req, res) {
		var record = req.body;

		scheduler(req.sessionId).insert(record, req.sessionId).then(function (result) {
			res.json(handler.response(result));
		}, function( err ) {
			logger.error( '[33]', err );
			logger.error( '[34]error by adding new scheduled task' );

			res.json(handler.error('Error adding schedule task', err));
		});
	});

	/*
	 * update scheduled task
	 */
	router.put('/:id', function( req, res ) {
		var record = req.body;

		scheduler(req.sessionId).update(req.params.id, record, req.sessionId).then(function (result) {
			res.json(handler.response(result));
		}, function( err ) {
			logger.error( '[49]', err );
			logger.error( '[50]error by update scheduled task' );

			res.json(handler.error('Error updating schedule task', err));
		});
	});

	/*
	 * delete scheduled task
	 */
	router.delete('/:id', function (req, res) {
		scheduler(req.sessionId).delete( req.params.id, req.sessionId).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			logger.error( '[63]', err );
			logger.error( '[64]error by delete scheduled task' );

			res.json(handler.error('schedule deletion error', err));
		});
	});

	/*
	 * enable scheduled task
	 */
	router.put( '/:id/enable', function( req, res ) {
		// logger.debug( '[74]/:id/enable' );
		// logger.debug( '[75]req: {', req, '}' );
		// logger.debug( '[76]res: {', res, '}' );
		// logger.debug( '[77]req.params.id: {', req.params.id, '}' );
		// logger.debug( '[78]req.sessionId: {', req.sessionId, '}' );

		scheduler( req.sessionId ).enable( req.params.id, req.sessionId).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			logger.error( '[83]', err );
			logger.error( '[84]error by enable scheduled task' );

			res.json(handler.error( 'schedule enable error', err ));
		});
	});

	/*
	 * disable scheduled task
	 */
	router.put( '/:id/disable', function( req, res ) {
		scheduler( req.sessionId ).disable( req.params.id, req.sessionId ).then(function (result) {
			res.json(handler.response(result));
		}, function( err ) {
			logger.error( '[91]', err );
			logger.error( '[92]error by disable scheduled task' );

			res.json(handler.error( 'schedule disable error', err ));
		});
	});

	module.exports = function (resHandler) {
		handler = resHandler;

		return router;
	};
} )( module );
