'use strict';

(function( module ) {
	var Data = require('src/node/com/data');

	module.exports = {
		response: function (res, additions) {
			var response = new Data.JSON_Response({
				response: res,
				error:    false,
				data:     additions
			});

			return response.data();
		},

		error: function (res, additions) {
			var response = new Data.JSON_Response({
				response: res,
				error:    true,
				data:     additions
			});

			return response.data();
		}
	};
} )( module );
