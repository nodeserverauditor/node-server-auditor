'use strict';

(function( module ) {
	function sessionParser(req, res, next) {
		if ( 'sessionId' in req ) {
			return next();
		}

		req.sessionId = req.header('X-Session-ID');

		if (req.sessionId == null) {
			req.sessionId = '';
		}

		next();
	}

	module.exports = sessionParser;
} )( module );
