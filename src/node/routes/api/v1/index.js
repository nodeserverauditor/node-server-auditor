'use strict';

(function (module, process, Date) {
	var express       = require('express');
	var resHandler    = require('./jsonResponseFactory');
	var reports       = require('./reports/')(resHandler);
	var config        = require('./config/')(resHandler);
	var language      = require('src/node/com/language');
	var sessionParser = require('./sessionParser');
	var scheduler     = require('./scheduler/')(resHandler);
	var paramsrep     = require('./paramsrep/')(resHandler);
	var router        = express.Router();

	router.use('/reports',   sessionParser, reports);
	router.use('/config',    sessionParser, config);
	router.use('/schedule',  sessionParser, scheduler);
	router.use('/paramsrep', sessionParser, paramsrep);

	router.get('/now', function (req, res) {
		res.json(resHandler.response(Date.now(), {measure: 'ms'}));
	});

	router.get('/languages', function (req, res) {
		res.json(resHandler.response(language.list()));
	});

	router.get('/languages/:name', function (req, res) {
		var lang = language.get(req.params.name);

		if (lang !== false) {
			res.json(resHandler.response(lang));
		} else {
			res.status(404)
				.json(resHandler.response('not found'));
		}
	});

	router.get('/memory', function (req, res) {
		res.json(resHandler.response(process.memoryUsage()));
	});

	router.get('/', function (req, res) {
		res.json(resHandler.response('It is not implemented'));
	});

	module.exports = router;
})(module, process, Date);
