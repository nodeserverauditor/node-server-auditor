'use strict';

(function( module ) {
	var handler   = null;
	var express   = require('express');
	var router    = express.Router();
	var paramsrep = require('src/node/com/paramsrep');

	router.get('/:name', function (req, res) {
		var sessionId = req.sessionId;

		paramsrep(req.sessionId).getParamsReport(req.params.name, sessionId).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error( 'Error get params', err ));
		});
	});

	router.put('/save', function (req, res) {
		var records = req.body;

		paramsrep(req.sessionId).save(records, req.sessionId).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error( 'Error save record', err ));
		});
	});

	module.exports = function (resHandler) {
		handler = resHandler;

		return router;
	};
} )( module );
