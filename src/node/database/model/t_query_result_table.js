'use strict';

(function( module ) {
	var Sequelize               = require( 'sequelize' );
	var main_storage_database   = require( 'src/node/database' )();
	var t_query                 = require( './t_query' );
	var t_query_result_database = require( './t_query_result_database' );

	var TQueryResultTable = main_storage_database.define( 't_query_result_table', {
		// column 'id_query_result_table'
		id_query_result_table: {
			field:         'id_query_result_table',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'result table identificator - primary key'
		},

		// column 'id_query'
		id_query: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_query,

				// This is the column name of the referenced model
				key: 'id_query'
			}
		},

		// column 'id_recordset'
		id_recordset: {
			field:         'id_recordset',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			comment:       'record set for the query',
			validate:      {
			}
		},

		// column 'id_result_database'
		id_result_database: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_query_result_database,

				// This is the column name of the referenced model
				key: 'id_result_database'
			}
		},

		// column 'result_table_name'
		result_table_name: {
			field:     'result_table_name',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'table name with query results',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		}
	}, {
		// define the table's name
		tableName: 't_query_result_table',

		comment: 'query results information',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TQueryResultTable.belongsTo(
				models.t_query, {
					foreignKey: 'id_query'
				}
			);

			TQueryResultTable.belongsTo(
				models.t_query_result_database, {
					foreignKey: 'id_result_database'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_query_result_table',
			unique: true,
			fields: [
				'id_query',
				'id_recordset'
			]
		} ]
	} );

	module.exports = TQueryResultTable;
} )( module );
