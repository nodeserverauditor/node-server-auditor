'use strict';

/*
* Model of extended query location table
*/

(function( module ) {
	var Sequelize        = require( 'sequelize' );
	var default_database = require( 'src/node/database' )();
	var t_query          = require( './t_query' );

	var TQueryLocation = default_database.define( 't_query_location', {
		// column 'id_query_location'
		id_query_location: {
			field:         'id_query_location',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'query location identificator - primary key',
			validate:      {
			}
		},

		// column 'id_query'
		id_query: {
			field:         'id_query',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			comment:       'fk to the query table',
			references:    {
				model: t_query,
				key:   'id_query'
			}
		},

		// column 'version_min'
		version_min: {
			field:         'version_min',
			type:          Sequelize.STRING,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			comment:       'min version of db server',
			validate:      {
			}
		},

		// column 'version_max'
		version_max: {
			field:         'version_max',
			type:          Sequelize.STRING,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			comment:       'max version of db server',
			validate:      {
			}
		},

		// column 'query_location'
		query_location: {
			field:         'query_location',
			type:          Sequelize.STRING,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			comment:       'query_location - local path to the file with the query',
			validate:      {
				notEmpty: true // don't allow empty strings
			}
		}
	}, {
		table_name: 't_query_location',

		comment: 'list of files with queries',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TQueryLocation.belongsTo(
				models.t_query, {
					foreignKey: 'id_query'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_query_version',
			unique: true,
			fields: [
				'id_query',
				'version_min',
				'version_max'
			]
		} ]
	} );

	module.exports = TQueryLocation;
} )( module );
