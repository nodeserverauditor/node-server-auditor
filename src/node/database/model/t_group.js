'use strict';

( function( module ) {
	var Sequelize         = require( 'sequelize' );
	var database          = require( 'src/node/database' )();
	var t_connection_type = require( './t_connection_type' );

	// table t_group
	var TGroup = database.define( 't_group', {
		// column 'id_group'
		id_group: {
			field:         'id_group',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'group identificator - primary key',
			validate:      {
			}
		},

		// column 'id_connection_type'
		id_connection_type: {
			type: Sequelize.INTEGER,

			references: {
				// it is the reference to another model
				model: t_connection_type,
				// it is the column name of the referenced model
				key: 'id_connection_type'
			}
		},

		// column 'group_name'
		group_name: {
			field:         'group_name',
			type:          Sequelize.STRING,
			allowNull:     false,
			comment:       'name of connection group',
			validate:      {
				notEmpty: true //don't allow empty string
			}
		},

		// column 'is_active'
		is_active: {
			field:        'is_active',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			comment:      'for soft delete of row',
			defaultValue: true
		}
	}, {
		// define the table's name
		tableName: 't_group',

		comment: 'list of connection groups',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TGroup.belongsTo(
				models.t_connection_type, {
					foreignKey: 'id_connection_type'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_group_group_name',
			unique: true,
			fields: [
				'id_connection_type',
				'group_name'
			]
		} ]
	} );

	TGroup.sync( {
		force: false
	} );

	module.exports = TGroup;
} )( module );
