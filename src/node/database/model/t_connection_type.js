'use strict';

(function( module ) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TConnectionType = module.exports = default_database.define('t_connection_type', {
		// column 'id_connection_type'
		id_connection_type: {
			field:         'id_connection_type',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'connection type identificator - primary key',
			validate:      {
			}
		},

		// column 'connection_type_name'
		connection_type_name: {
			field:     'connection_type_name',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'connection type name - text description',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		}
	}, {
		// define the table's name
		tableName: 't_connection_type',

		comment: 'list of availible connection types, supported by application',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
		},

		indexes: [ {
			name:   'uidx_t_connection_type_connection_type_name',
			unique: true,
			fields: [
				'connection_type_name'
			]
		} ]
	} );

	module.exports = TConnectionType;
} )( module );
