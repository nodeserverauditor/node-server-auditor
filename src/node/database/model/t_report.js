'use strict';

/*
* Model of the report table
*/

(function( module ) {
	var Sequelize        = require( 'sequelize' );
	var default_database = require( 'src/node/database' )();
	var t_module         = require( './t_module' );

	var TReport = default_database.define( 't_report', {
		// column 'id_report'
		id_report: {
			field:         'id_report',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'report identificator - primary key',
			validate:      {
			}
		},

		// column 'id_module'
		id_module: {
			type:          Sequelize.INTEGER,
			primaryKey:    false,
			autoIncrement: false,

			references: {
				// This is a reference to another model
				model: t_module,

				// This is the column name of the referenced model
				key: 'id_module'
			}
		},

		// column 'report_name'
		report_name: {
			field:         'report_name',
			type:          Sequelize.STRING,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			comment:       'name of report from the configuration file',
			validate:      {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'report_location'
		report_location: {
			field:         'report_location',
			type:          Sequelize.STRING,
			allowNull:     true,
			primaryKey:    false,
			autoIncrement: false,
			comment:       'location of the report file',
			validate:      {
				notEmpty: false // allow empty strings
			}
		},

		// column 'is_active'
		is_active: {
			field:         'is_active',
			type:          Sequelize.BOOLEAN,
			allowNull:     false,
			primaryKey:    false,
			autoIncrement: false,
			defaultValue:  true,
			comment:       'true if report is still active (exists)',
			validate:      {
			}
		}
	}, {
		// define the table's name
		tableName: 't_report',

		comment: 'list of reports',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TReport.belongsTo(
				models.t_module, {
					foreignKey: 'id_module'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_report_report_name',
			unique: true,
			fields: [
				'id_module',
				'report_name'
			]
		} ]
	} );

	module.exports = TReport;
} )( module );
