'use strict';

(function( module ) {
	var Sequelize = require('sequelize');
	var config    = require('configuration');
	var logger    = require('src/node/log');
	var dbconfs   = config.databases;
	var databases = {};

	for ( var dbtitle in dbconfs ) {
		// logger.debug( '[11]index.js:dbtitle: {', dbtitle, '}' );

		var dbconf = dbconfs[dbtitle];

		// logger.debug( '[15]index.js:dbconf: {', dbconf, '}' );
		// logger.debug( '[16]index.js:dbconf.filename: {', dbconf.filename, '}' );

		databases[dbtitle] = new Sequelize(
			dbconf.basename,
			dbconf.user,
			dbconf.password, {
				dialect: 'sqlite',
				logging: false,
				pool:    {
					max:  5,
					min:  0,
					idle: 10000
				},
				storage: dbconf.filename
			}
		);
	}

	module.exports = function (dbtitle) {
		// logger.debug( '[35]index.js:dbtitle: {', dbtitle, '}' );
		// logger.debug( '[36]index.js:return value: {', databases[ dbtitle == null ? 'default' : dbtitle ], '}' );

		return databases[ dbtitle == null ? 'default' : dbtitle ];
	};
} )( module );
