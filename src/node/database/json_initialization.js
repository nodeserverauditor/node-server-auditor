'use strict';

/*
 * Module, that initialized main.sqlite
 * from json config file
 */

( function( module ) {
	var Q         = require( 'q' );
	var Sequelize = require( 'sequelize' );
	var Promise   = require( 'promise' );
	var path      = require( 'path' );
	var logger    = require( 'src/node/log' );
	var dbManager = require( '../../../src/node/database' );
	var databases = require( '../../../configuration' ).databases;
	var appDir    = path.dirname( require.main.filename );
	var _         = require( 'lodash' );

	/*
	Read config and get files
	to initialize
	*/
	function buildInitializeScript() {
		var local_data_sorage = Object.keys( databases );
		var initConfig        = null;
		var initFile          = null;
		var scriptsFolder     = null;
		var sequences         = [];

		local_data_sorage.forEach( function( dbName ) {
			initFile = databases[dbName].initialization;

			// logger.debug( '[33]initFile: {', initFile, '}' );

			if ( initFile ) {
				initConfig = require( initFile );

				sortByOrder( initConfig );

				scriptsFolder = path.dirname( initFile );

				// try initialize table
				sequences = sequences.concat( initConfig.map( function( row ) {
					return initTable( scriptsFolder, row.script, dbName );
				} ) );

				return dbManager( dbName ).sync().then( function() {
					return Q.resolve();
				} );
			}
		} );
	}

	/*
	function, that try to create
	table by json file
	*/
	function initTable( scriptFolder, scriptFile, dbName ) {
		var fullScriptFileName = path.normalize( path.join( appDir, scriptFolder, scriptFile ) );
		var tableScript        = require( fullScriptFileName );
		var tableName          = null;
		var sqType             = null;
		var fields             = null;

		// logger.debug( '[65]scriptFolder: {', scriptFolder, '}' );
		// logger.debug( '[66]scriptFile: {', scriptFile, '}' );
		// logger.debug( '[67]dbName: {', dbName, '}' );

		if ( tableScript ) {
			// try to define table in db
			tableName = tableScript[1].tableName;

			// logger.debug( '[73]tableName: {', tableName, '}' );

			fields = Object.keys( tableScript[0] );

			for ( var i = 0; i < fields.length; i++ ) {
				if ( fields[i] in tableScript[0] ) {
					sqType = getSequelizeType( tableScript[0][fields[i]].type );

					if ( sqType != null ) {
						tableScript[0][fields[i]].type = sqType;
					}
				}
			}

			if ( tableName ) {
				dbManager( dbName ).define( tableName, _.clone( tableScript[0] ), _.clone( tableScript[1] ) );

				return true
			}
		}
	}

	/*
	sorting files by order from config
	*/
	function sortByOrder( data ) {
		data.sort( function( a, b ) {
			return Number( a.order ) - Number( b.order );
		} )
	}

	/*
	return Sequelize type of row
	*/
	function getSequelizeType( strType ) {
		var ret = null;

		switch ( strType ) {
			case 'Sequelize.STRING':
				ret = Sequelize.STRING;
				break;
			case 'Sequelize.TEXT':
				ret = Sequelize.TEXT;
				break;
			case 'Sequelize.INTEGER':
				ret = Sequelize.INTEGER;
				break;
			case 'Sequelize.DATE':
				ret = Sequelize.DATE;
				break;
			case 'Sequelize.BIGINT':
				ret = Sequelize.BIGINT;
				break;
			case 'Sequelize.REAL':
				ret = Sequelize.REAL;
				break;
			case 'Sequelize.DOUBLE':
				ret = Sequelize.DOUBLE;
				break;
			case 'Sequelize.DECIMAL':
				ret = Sequelize.DECIMAL;
				break;
			case 'Sequelize.BOOLEAN':
				ret = Sequelize.BOOLEAN;
				break;
			case 'Sequelize.DATEONLY':
				ret = Sequelize.DATEONLY;
				break;
			case 'Sequelize.BLOB':
				ret = Sequelize.BLOB;
				break;
			case 'Sequelize.FLOAT':
				ret = Sequelize.FLOAT;
				break;
			case 'Sequelize.JSON':
				ret = Sequelize.JSON;
				break;
			case 'Sequelize.JSONB':
				ret = Sequelize.JSONB;
				break;
			default:
				ret = null;
				break;
		}

		return ret;
	}

	module.exports = buildInitializeScript;
} )( module );
