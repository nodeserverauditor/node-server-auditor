'use strict';

(function( module ) {
	var logger = require('./logger.js').LOG;

	module.exports = logger;
} )( module );
