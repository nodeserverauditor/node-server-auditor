(function (app) {
	app.controller('schedulerCtrl', ['$scope', '$query', '$translate', '$uibModal', function ($scope, $query, $translate, $uibModal) {
		// Load info scheduler
		$scope.schedules = [];

		$query('schedule', {}).then(function (res) {
			var data = [];

			if ( res.response.length === 0 ) {
				throw new Error('data not found');
			}

			data = res.response[0];

			for ( var i = 0; i < data.length; i++ ) {
				data[i].status = data[i].isEnabled;
			}

			$scope.schedules = data;
		});

		$scope.deleteTask = function(record) {
			$query('schedule/' + record.id_connection_query_schedule, {}, 'DELETE' ).then(function (data) {
				for ( var i = 0; i < $scope.schedules.length; i++ ) {
					if ($scope.schedules[i] === record) {
						$scope.schedules.splice(i, 1);
						break;
					}
				}
			}, function (res) {
				alertify('error', 'alertScheduleError');
			});
		};

		$scope.addScheduleItemDialog = function (record) {
			var modalInstance = $uibModal.open({
				templateUrl: '/pages/schedCreate.html',
				controller:  'schedCreateCtrl',
				resolve: {
					record: function() {
						return record;
					}
				}
			});

			modalInstance.result.then(function (result) {
				if ( result ) {
					$scope.schedules.push(result);
				}
			}, function() {
			});
		};

		$scope.toggleStatus = function (record) {
			if (record.isEnabled) {
				$query('schedule/' + record.id_connection_query_schedule + '/disable', {}, 'PUT' ).then(function (data) {
					record.isEnabled = false;
				}, function (res) {
				});
			} else {
				$query('schedule/' + record.id_connection_query_schedule + '/enable', record, 'PUT' ).then(function (data) {
					record.isEnabled = true;
				}, function (res) {
				});
			}
		};
	}]);
})(app);
