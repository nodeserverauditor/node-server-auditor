/* global app */

/**
 * Tablesorter Directive
 */
app.directive('tablesorter', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		scope: {},
		link: function ($scope, element, attrs) {
			$timeout(function() {
				element.tablesorter({
					theme          : 'bootstrap',
					widgets        : ['uitheme'],
					headerTemplate : '{content} {icon}'
				});
			});
		}
	};
}]);
