/* global angular */

/**
 * Graph Bar Component
 *
 * @requires BaseGraphComp
 * @requires Collection
 * @requires GraphBarData
 */
app.factory('GraphBarComp', ['BaseGraphComp', 'Collection', 'GraphBarData', function (BaseGraphComp, Collection, GraphBarData) {
	var GraphBarComp = function() {
		BaseGraphComp.call(this);

		angular.merge(this.options, {
			series: [ {
					renderer: $.jqplot.BarRenderer
				}
			],
			axes: {
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer
				}
			},
			legend: {
				show:     true, // show legened
				location: 'ne', // compass direction, nw, n, ne, e, se, s, sw, w.
				xoffset:  12,   // pixel offset of the legend box from the x (or x2) axis.
				yoffset:  12    // pixel offset of the legend box from the y (or y2) axis.
			}
		});
	};

	angular.extend(GraphBarComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function (data) {
			var graphLine = new Collection(GraphBarData, data);

			return [graphLine.data()];
		}
	});

	return GraphBarComp;
}]);
