/* global angular */

/**
 * Two Lines Report Component
 *
 * @requires BaseGraphComp
 * @requires GraphLineData
 * @requires TwoXYLinesCollection
 */
app.factory('TwoXYLinesComp', ['BaseGraphComp', 'GraphLineData', 'TwoXYLinesCollection', function (BaseGraphComp, GraphLineData, TwoXYLinesCollection) {
	var TwoXYLinesComp = function() {
		BaseGraphComp.call(this);
	};

	angular.extend(TwoXYLinesComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function (data) {
			var graphLine = new TwoXYLinesCollection(GraphLineData, data);

			return graphLine.data();
		}
	});

	return TwoXYLinesComp;
}]);
