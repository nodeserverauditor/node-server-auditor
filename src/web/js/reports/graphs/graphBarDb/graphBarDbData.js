/* global angular, app */

/**
 * Graph Bar DataBase Data Model
 *
 * @requires Data
 */
app.factory('GraphBarDbData', ['Data', function (Data) {
	var GraphBarData = function( params ) {
		this.row = [params.DatabaseName, params.DatabaseSizeMB];
	};

	angular.extend(GraphBarData.prototype, Data.prototype, {
		data: function() {
			return this.row;
		}
	});

	return GraphBarData;
}]);
