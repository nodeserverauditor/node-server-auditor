/* global angular, app */

/**
 * Table Raw Component
 *
 * @requires TableRawDataset
 */
app.factory('TableRawComp', ['TableRawDataset', function (TableRawDataset) {
	var TableRawComp = function() {
	};

	angular.extend(TableRawComp.prototype, {
		updateReportData: function (data) {
			var table = new TableRawDataset(data);

			return table.data();
		}
	});

	return TableRawComp;
}]);
