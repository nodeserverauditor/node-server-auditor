/* global app */
/**
 * Table Raw Directive
 *
 * @requires TableRawComp
 */
app.directive('tableRawReport', ['TableRawComp', function (TableRawComp) {
	return {
		restrict: 'EA',
		templateUrl: 'directives/report/tables/table-raw.html',
		scope: {
			reportItem: '=tableRawReport'
		},
		link: function ($scope, element, attrs) {
			var table = new TableRawComp();

			$scope.preparedData = table.updateReportData($scope.reportItem);

			$scope.$watch('reportItem', function ( newValue, oldValue ) {
				if ( newValue !== oldValue ) {
					$scope.preparedData = table.updateReportData(newValue);
				}
			});
		}
	};
}]);
