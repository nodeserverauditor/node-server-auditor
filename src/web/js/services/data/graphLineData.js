/* global angular, app */

app.factory('GraphLineData', ['Data', function (Data) {
	var GraphLineData = function( params ) {
		this.row = [params.x, params.y];
	};

	angular.extend(GraphLineData.prototype, Data.prototype, {
		data: function() {
			return this.row;
		}
	});

	return GraphLineData;
}]);
