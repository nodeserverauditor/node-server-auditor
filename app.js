'use strict';

(function (module, console) {
	var express    = require( 'express' );
	var bodyParser = require( 'body-parser' );
	var path       = require( 'path' );
	var config     = require( './configuration' );
	var api        = require( 'src/node/routes/api/v1/' );
	var dbConfig   = require( 'src/node/database/initial' );
	var logger     = require( 'src/node/log' );

	dbConfig.init()
		.then ( runApp )
		.catch( function( err ) {
			logger.error( '[15]dbConfig.init()', err );

			console.error( err );

			process.exit( -1 );
		})

	function runApp() {
		var app = express();

		// to support JSON-encoded bodies
		app.use(bodyParser.json());

		app.use(bodyParser.urlencoded( {
			// to support URL-encoded bodies
			extended: true
		}));

		app.use(express.static( 'web' ));

		app.use( '/api/v1', api );

		app.use( function( req, res ) {
			// res.sendFile(__dirname + '/web/');
			res.sendFile( path.join( __dirname, 'web') );
		});

		app.listen( config.port, function() {
			logger.info( 'Express node.js server started' );
			logger.info( 'Express node.js server listening on port: {', config.port, '}' );

			console.log( 'Express node.js server listening on port: ', config.port );
		});
	}
})(module, console);
