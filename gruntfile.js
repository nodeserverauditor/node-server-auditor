'use strict';

module.exports = function( grunt ) {
	var clone = require('clone');
	var pkg;
	var bowerOptions;
	var bowerOptionsDev;

	var paths = {
		lib:      'web/lib/',
		libJS:    'web/lib/js/',
		libCSS:   'web/lib/css/',
		libFonts: 'web/lib/fonts/',
		dest:     'web/'
	};

	pkg = grunt.file.readJSON('package.json');

	bowerOptions = {
		dest: paths.libJS + 'bower.js',
		cssDest: paths.libCSS + 'bower.css',
		exclude: [],
		mainFiles: {
			'bootswatch-dist': [
				'js/bootstrap.min.js',
				'css/bootstrap.min.css'
			],
			jqplot: [
				'jquery.jqplot.min.js',
				'jquery.jqplot.min.css',
				'plugins/jqplot.barRenderer.min.js',
				'plugins/jqplot.dateAxisRenderer.min.js',
				'plugins/jqplot.categoryAxisRenderer.min.js',
				'plugins/jqplot.canvasAxisTickRenderer.min.js',
				'plugins/jqplot.canvasTextRenderer.min.js'
			],
			tablesorter: [
				'dist/css/theme.bootstrap.min.css',
				'dist/js/jquery.tablesorter.min.js',
				'dist/js/jquery.tablesorter.widgets.min.js'
			]
		},
		dependencies: {
			'bootstrap-css': 'jquery'
		},
		callback: function( mainFiles, component ) {
			return mainFiles.map(function( filepath ) {
				// Use minified files if available
				var min = filepath.replace(/\.js$/, '.min.js');
				return grunt.file.exists(min) ? min : filepath;
			});
		}
	};

	bowerOptionsDev = clone(bowerOptions);
	delete bowerOptionsDev.callback;

	grunt.initConfig({
		pkg: pkg,
		clean: [paths.dest],
		uglify: {
			dev: {
				options: {
					banner:           '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n/*! Developer mode */\n',
					beautify:         true,
					mangle:           false,
					preserveComments: 'all'
				},
				files: [{
						src: [
							'src/web/js/com/**/*.js',
							'src/web/js/app.js',
							'src/web/js/services/**/*.js',
							'src/web/js/directives/**/*.js',
							'src/web/js/controllers/**/*.js',
							'src/web/js/reports/**/*.js'
						],
						dest: paths.dest + 'main.min.js'
					}]
			},
			build: {
				options: {
					banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
					preserveComments: 'all'
				},
				files: [{
						src: [
							'src/web/js/com/**/*.js',
							'src/web/js/app.js',
							'src/web/js/services/**/*.js',
							'src/web/js/directives/**/*.js',
							'src/web/js/controllers/**/*.js',
							'src/web/js/reports/**/*.js'
						],
						dest: paths.dest + 'main.min.js'
					}]
			}
		},
		bower_concat: {
			build: bowerOptions,
			dev: bowerOptionsDev
		},
		jade: {
			compile: {
				options: {
					pretty: true,
					data: {
						pkg: pkg
					}
				},
				files: [{
						expand: true,
						src:    ['**/*.jade', '!includes/**/*'],
						dest:   paths.dest,
						cwd:    'src/web/jade/',
						ext:    '.html'
					}]
			}

		},
		less: {
			dev: {
				options: {
					compress: false
				},
				files: [{
						src: ['src/web/less/**/*.less'],
						dest: paths.dest + 'main.min.css'
					}]
			},
			build: {
				options: {
					compress: true
				},
				files: [{
						src: ['src/web/less/**/*.less'],
						dest: paths.dest + 'main.min.css'
					}]
			}
		},
		watch: {
			dev: {
				files: ['src/web/**/*'],
				tasks: ['dev-watch']
			},
			build: {
				files: ['src/web/**/*'],
				tasks: ['build-watch']
			}
		},
		copy: {
			main: {
				files: [
					{expand: true, cwd: 'src/web/', src: ['fonts/*', 'images/*'], dest: paths.lib},
					{expand: true, cwd: 'bower_components/bootswatch-dist/fonts/', src: ['*'], dest: paths.libFonts},
					{expand: true, cwd: 'bower_components/components-font-awesome/fonts/', src: ['*'], dest: paths.libFonts}
				],
			},
		}

	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-bower-concat');

	grunt.registerTask('default', ['dev', 'watch:dev']);
	grunt.registerTask('default:build', ['build', 'watch:build']);

	grunt.registerTask('dev', ['tasks', 'bower_concat:dev', 'uglify:dev', 'less:dev', 'fileCollector', 'jade']);
	grunt.registerTask('build', ['tasks', 'bower_concat:build', 'uglify:build', 'less:build', 'fileCollector', 'jade']);

	grunt.registerTask('dev-watch', ['copy', 'uglify:dev', 'less:dev', 'fileCollector', 'jade']);
	grunt.registerTask('build-watch', ['copy', 'uglify:build', 'less:build', 'fileCollector', 'jade']);

	grunt.registerTask('tasks', ['clean', 'copy']);

	grunt.registerTask('fileCollector', 'collects files', function() {
		var jade = grunt.config.get('jade');
		var fs   = require('fs');

		jade.compile.options.data.libs = {};

		if (fs.existsSync(paths.libJS)) {
			jade.compile.options.data.libs.js = fs.readdirSync(paths.libJS).filter(function( item ) {
				return /^[\w\-.]+.js$/.test(item);
			});
		}

		if (fs.existsSync(paths.libCSS)) {
			jade.compile.options.data.libs.css = fs.readdirSync(paths.libCSS).filter(function( item ) {
				return /^[\w\-.]+.css$/.test(item);
			});
		}

		grunt.config.set('jade', jade);
	});
};
